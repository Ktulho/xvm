package net.wg.data.constants.generated
{
    public class HANGAR_ALIASES extends Object
    {

        public static const TANK_CAROUSEL:String = "tankCarousel";

        public static const RANKED_TANK_CAROUSEL:String = "rankedTankCarousel";

        public static const EPICBATTLE_TANK_CAROUSEL:String = "epicBattleTankCarousel";

        public static const BATTLEPASS_TANK_CAROUSEL:String = "battlePassTankCarousel";

        public static const CREW:String = "crew";

        public static const AMMUNITION_PANEL:String = "ammunitionPanel";

        public static const RESEARCH_PANEL:String = "researchPanel";

        public static const TMEN_XP_PANEL:String = "tmenXpPanel";

        public static const VEHICLE_PARAMETERS:String = "params";

        public static const HEADER:String = "header";

        public static const RANKED_WIDGET:String = "rankedWdgt";

        public static const ALERT_MESSAGE_BLOCK:String = "alertMessageBlock";

        public static const EPIC_WIDGET:String = "epicWdgt";

        public static const SENIORITY_AWARDS_ENTRY_POINT:String = "SENIORITY_AWARDS_ENTRY_POINT";

        public static const RANKED_PRIME_TIME:String = "rankedPrimeTime.swf";

        public static const EPIC_PRIME_TIME:String = "epicPrimeTime.swf";

        public static const EVENT_10YC_PRIME_TIME:String = "event10YCPrimeTime.swf";

        public static const TANK_CAROUSEL_UI:String = "TankCarouselUI";

        public static const VEH_PARAM_RENDERER_STATE_SIMPLE_TOP:String = "simpleTop";

        public static const VEH_PARAM_RENDERER_STATE_SIMPLE_BOTTOM:String = "simpleBottom";

        public static const VEH_PARAM_RENDERER_STATE_ADVANCED:String = "advanced";

        public static const VEH_PARAM_RENDERER_STATE_SEPARATOR:String = "separator";

        public static const PROGRESSIVE_REWARD_WIDGET:String = "progressiveRewardWdgt";

        public static const DAILY_QUEST_WIDGET:String = "dqWidget";

        public static const BATTLE_PASSS_ENTRY_POINT:String = "battlePassEntryPoint";

        public static const TEN_YEARS_COUNTDOWN_ENTRY_POINT_INJECT:String = "tenYearsCountdownEntryPointInject";

        public function HANGAR_ALIASES()
        {
            super();
        }
    }
}
