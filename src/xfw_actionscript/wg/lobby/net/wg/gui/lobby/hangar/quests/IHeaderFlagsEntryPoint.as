package net.wg.gui.lobby.hangar.quests
{
    import net.wg.infrastructure.interfaces.IUIComponentEx;

    public interface IHeaderFlagsEntryPoint extends IUIComponentEx
    {

        function get marginX() : int;
    }
}
