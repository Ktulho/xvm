package net.wg.gui.lobby.questsWindow.components.interfaces
{
    import net.wg.infrastructure.interfaces.IViewStackContent;
    import net.wg.infrastructure.interfaces.IFocusChainContainer;

    public interface IComplexViewStackItem extends IViewStackContent, IFocusChainContainer
    {
    }
}
