package net.wg.gui.lobby.profile
{
    public class ProfileInvalidationTypes extends Object
    {

        public static const RARE_ACHMNT_RECEIVED_DATA_INV:String = "achievementRequestInv";

        public function ProfileInvalidationTypes()
        {
            super();
        }
    }
}
