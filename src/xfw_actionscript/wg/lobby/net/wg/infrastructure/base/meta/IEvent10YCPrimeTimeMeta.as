package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IEvent10YCPrimeTimeMeta extends IEventDispatcher
    {

        function as_setTitle(param1:String) : void;

        function as_setBg(param1:String) : void;
    }
}
