package
{
    public class DEMOUNT_KIT extends Object
    {

        public static const USERNAME_COMMON:String = "#demount_kit:userName/common";

        public static const DEMOUNTKIT_TOOLTIP_DESCRIPTION_COMMON:String = "#demount_kit:demountKit/tooltip/description/common";

        public static const STORAGE_USERNAME_COMMON:String = "#demount_kit:storage/userName/common";

        public static const STORAGE_DESCRIPTION_COMMON:String = "#demount_kit:storage/description/common";

        public static const DIALOGUE_DESCRIPTION_COMMON:String = "#demount_kit:dialogue/description/common";

        public static const EQUIPMENTINSTALL_DEMOUNTDESCRIPTION_COMMON:String = "#demount_kit:equipmentInstall/demountDescription/common";

        public static const EQUIPMENTINSTALL_DEMOUNTDESCRIPTION_BONDS:String = "#demount_kit:equipmentInstall/demountDescription/bonds";

        public static const EQUIPMENTINSTALL_DEMOUNTDESCRIPTION_TROPHY:String = "#demount_kit:equipmentInstall/demountDescription/trophy";

        public static const EQUIPMENTDEMOUNT_OPTIONFREE:String = "#demount_kit:equipmentDemount/optionFree";

        public static const EQUIPMENTDEMOUNT_CONFIRMATION:String = "#demount_kit:equipmentDemount/confirmation";

        public static const EQUIPMENTDEMOUNT_CONFIRMATION_DESCRIPTION:String = "#demount_kit:equipmentDemount/confirmation/description";

        public static const EQUIPMENTDEMOUNTPRICE:String = "#demount_kit:equipmentDemountPrice";

        public static const DEMOUNTCONFIRMATION_SUBMIT:String = "#demount_kit:demountConfirmation/submit";

        public static const EQUIPMENTDEMOUNT_NOTENOUGHBONDS_TEXT:String = "#demount_kit:equipmentDemount/notEnoughBonds/text";

        public static const EQUIPMENTDEMOUNT_NOTENOUGHBONDS_DESCRIPTION:String = "#demount_kit:equipmentDemount/notEnoughBonds/description";

        public static const SHOP_DEMOUNTOPTIONS_COMMON:String = "#demount_kit:shop/DemountOptions/common";

        public static const SHOP_DEMOUNTOPTIONS_BONDS:String = "#demount_kit:shop/DemountOptions/bonds";

        public static const DEMOUNTKIT_GAINED_COUNT:String = "#demount_kit:demountKit/gained/count";

        public static const DEMOUNTKIT_SELL_SUCCESS:String = "#demount_kit:demountKit/sell_success";

        public static const TOOLTIPS_VEHICLE_ACTION_PRC:String = "#demount_kit:tooltips:vehicle/action_prc";

        public static const DISCOUNTS_TOOLTIPS_NOSALEPROFIT:String = "#demount_kit:discounts/tooltips/noSaleProfit";

        public static const DEMOUNTKIT_SELL_HEADER:String = "#demount_kit:demountKit/sell/header";

        public function DEMOUNT_KIT()
        {
            super();
        }
    }
}
