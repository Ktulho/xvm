package net.wg.data.constants.generated
{
    public class BATTLEATLAS extends Object
    {

        public static const ACCEPT_TO_SQUAD_HOVER:String = "accept_to_squad_hover";

        public static const ACCEPT_TO_SQUAD_NORMAL:String = "accept_to_squad_normal";

        public static const ADD_TO_SQUAD_NORMAL:String = "add_to_squad_normal";

        public static const ADD_TO_SQUAD_OVER:String = "add_to_squad_over";

        public static const AIRSTRIKE_ENEMY:String = "airstrikeEnemy";

        public static const AIRSTRIKE_ENEMY_BLIND:String = "airstrikeEnemyBlind";

        public static const AIRSTRIKE_ENEMY_CRITS:String = "airstrikeEnemyCrits";

        public static const AIRSTRIKE_ENEMY_CRITS_BLIND:String = "airstrikeEnemyCritsBlind";

        public static const ALLIED_ATSPG_MS:String = "allied_atspg-ms";

        public static const ALLIED_ATSPG_TS:String = "allied_atspg-ts";

        public static const ALLIED_SPG_MS:String = "allied_spg-ms";

        public static const ALLIED_SPG_TS:String = "allied_spg-ts";

        public static const ALLIED_TANK_SH:String = "allied_tank_sh";

        public static const ALLIED_TANK_SH_DEAD:String = "allied_tank_sh_dead";

        public static const ALLY_ABSORPTION_FLAG_ANIM_ENTRY:String = "AllyAbsorptionFlagAnimEntry";

        public static const ALLY_ABSORPTION_FLAG_ENTRY:String = "AllyAbsorptionFlagEntry";

        public static const ALLY_REPAIR_ACTIVE_ENTRY:String = "AllyRepairActiveEntry";

        public static const ALLY_REPAIR_COOLDOWN_ENTRY:String = "AllyRepairCooldownEntry";

        public static const ALLY_TEAM_BASE_ENTRY_GREEN_0:String = "AllyTeamBaseEntry_green_0";

        public static const ALLY_TEAM_BASE_ENTRY_GREEN_1:String = "AllyTeamBaseEntry_green_1";

        public static const ALLY_TEAM_BASE_ENTRY_GREEN_2:String = "AllyTeamBaseEntry_green_2";

        public static const ALLY_TEAM_BASE_ENTRY_GREEN_3:String = "AllyTeamBaseEntry_green_3";

        public static const ALLY_TEAM_SPAWN_ENTRY_GREEN_1:String = "AllyTeamSpawnEntry_green_1";

        public static const ALLY_TEAM_SPAWN_ENTRY_GREEN_2:String = "AllyTeamSpawnEntry_green_2";

        public static const ALLY_TEAM_SPAWN_ENTRY_GREEN_3:String = "AllyTeamSpawnEntry_green_3";

        public static const ALLY_TEAM_SPAWN_ENTRY_GREEN_4:String = "AllyTeamSpawnEntry_green_4";

        public static const AMMO_BAY:String = "ammoBay";

        public static const AMMO_BAY_ORANGE:String = "ammoBay_orange";

        public static const AMMO_BAY_RED:String = "ammoBay_red";

        public static const ARTILLERY_ENEMY:String = "artilleryEnemy";

        public static const ARTILLERY_ENEMY_BLIND:String = "artilleryEnemyBlind";

        public static const ARTILLERY_ENEMY_CRITS:String = "artilleryEnemyCrits";

        public static const ARTILLERY_ENEMY_CRITS_BLIND:String = "artilleryEnemyCritsBlind";

        public static const ARTILLERY_ENTRY:String = "ArtilleryEntry";

        public static const AT_SPG_ALLY_DEAD_GREEN:String = "AT-SPG_ally_dead_green";

        public static const AT_SPG_ALLY_GREEN:String = "AT-SPG_ally_green";

        public static const AT_SPG_ENEMY_DEAD_PURPLE:String = "AT-SPG_enemy_dead_purple";

        public static const AT_SPG_ENEMY_DEAD_RED:String = "AT-SPG_enemy_dead_red";

        public static const AT_SPG_ENEMY_PURPLE:String = "AT-SPG_enemy_purple";

        public static const AT_SPG_ENEMY_RED:String = "AT-SPG_enemy_red";

        public static const AT_SPG_SQUADMAN_DEAD_GOLD:String = "AT-SPG_squadman_dead_gold";

        public static const AT_SPG_SQUADMAN_DEAD_YELLOW:String = "AT-SPG_squadman_dead_yellow";

        public static const AT_SPG_SQUADMAN_GOLD:String = "AT-SPG_squadman_gold";

        public static const AT_SPG_SQUADMAN_YELLOW:String = "AT-SPG_squadman_yellow";

        public static const AT_SPG_TEAM_KILLER_BLUE:String = "AT-SPG_teamKiller_blue";

        public static const AT_SPG_TEAM_KILLER_DEAD_BLUE:String = "AT-SPG_teamKiller_dead_blue";

        public static const BADGE_1:String = "badge_1";

        public static const BADGE_10:String = "badge_10";

        public static const BADGE_11:String = "badge_11";

        public static const BADGE_12:String = "badge_12";

        public static const BADGE_13:String = "badge_13";

        public static const BADGE_14:String = "badge_14";

        public static const BADGE_15:String = "badge_15";

        public static const BADGE_16:String = "badge_16";

        public static const BADGE_17:String = "badge_17";

        public static const BADGE_18:String = "badge_18";

        public static const BADGE_19:String = "badge_19";

        public static const BADGE_2:String = "badge_2";

        public static const BADGE_20:String = "badge_20";

        public static const BADGE_21:String = "badge_21";

        public static const BADGE_22:String = "badge_22";

        public static const BADGE_23:String = "badge_23";

        public static const BADGE_24:String = "badge_24";

        public static const BADGE_25:String = "badge_25";

        public static const BADGE_26:String = "badge_26";

        public static const BADGE_27:String = "badge_27";

        public static const BADGE_28:String = "badge_28";

        public static const BADGE_29:String = "badge_29";

        public static const BADGE_3:String = "badge_3";

        public static const BADGE_30:String = "badge_30";

        public static const BADGE_31:String = "badge_31";

        public static const BADGE_32:String = "badge_32";

        public static const BADGE_33:String = "badge_33";

        public static const BADGE_34:String = "badge_34";

        public static const BADGE_35:String = "badge_35";

        public static const BADGE_36:String = "badge_36";

        public static const BADGE_37:String = "badge_37";

        public static const BADGE_38:String = "badge_38";

        public static const BADGE_39:String = "badge_39";

        public static const BADGE_4:String = "badge_4";

        public static const BADGE_40:String = "badge_40";

        public static const BADGE_41:String = "badge_41";

        public static const BADGE_42:String = "badge_42";

        public static const BADGE_43:String = "badge_43";

        public static const BADGE_44:String = "badge_44";

        public static const BADGE_45:String = "badge_45";

        public static const BADGE_46:String = "badge_46";

        public static const BADGE_47:String = "badge_47";

        public static const BADGE_48:String = "badge_48";

        public static const BADGE_49:String = "badge_49";

        public static const BADGE_5:String = "badge_5";

        public static const BADGE_50:String = "badge_50";

        public static const BADGE_51:String = "badge_51";

        public static const BADGE_52:String = "badge_52";

        public static const BADGE_56:String = "badge_56";

        public static const BADGE_57:String = "badge_57";

        public static const BADGE_58:String = "badge_58";

        public static const BADGE_59:String = "badge_59";

        public static const BADGE_6:String = "badge_6";

        public static const BADGE_60:String = "badge_60";

        public static const BADGE_61:String = "badge_61";

        public static const BADGE_62:String = "badge_62";

        public static const BADGE_63:String = "badge_63";

        public static const BADGE_64:String = "badge_64";

        public static const BADGE_65:String = "badge_65";

        public static const BADGE_66:String = "badge_66";

        public static const BADGE_67:String = "badge_67";

        public static const BADGE_68:String = "badge_68";

        public static const BADGE_69:String = "badge_69";

        public static const BADGE_7:String = "badge_7";

        public static const BADGE_70:String = "badge_70";

        public static const BADGE_71:String = "badge_71";

        public static const BADGE_72:String = "badge_72";

        public static const BADGE_73:String = "badge_73";

        public static const BADGE_74:String = "badge_74";

        public static const BADGE_75:String = "badge_75";

        public static const BADGE_76:String = "badge_76";

        public static const BADGE_77:String = "badge_77";

        public static const BADGE_78:String = "badge_78";

        public static const BADGE_79:String = "badge_79";

        public static const BADGE_8:String = "badge_8";

        public static const BADGE_80:String = "badge_80";

        public static const BADGE_81:String = "badge_81";

        public static const BADGE_82:String = "badge_82";

        public static const BADGE_83:String = "badge_83";

        public static const BADGE_84:String = "badge_84";

        public static const BADGE_85:String = "badge_85";

        public static const BADGE_86:String = "badge_86";

        public static const BADGE_87:String = "badge_87";

        public static const BADGE_88:String = "badge_88";

        public static const BADGE_89:String = "badge_89";

        public static const BADGE_9:String = "badge_9";

        public static const BADGE_90:String = "badge_90";

        public static const BADGE_90_GOLD:String = "badge_90_gold";

        public static const BADGE_91:String = "badge_91";

        public static const BADGE_92:String = "badge_92";

        public static const BADGE_93:String = "badge_93";

        public static const BATTLE_LOADING_FORM_BG_TABLE:String = "battleLoadingFormBgTable";

        public static const BATTLE_LOADING_FORM_BG_TIPS:String = "battleLoadingFormBgTips";

        public static const BATTLE_LOADING_FORM_MAP_BACKGROUND:String = "battleLoadingFormMapBackground";

        public static const BATTLE_LOADING_FORM_TIP_BACKGROUND:String = "battleLoadingFormTipBackground";

        public static const BATTLE_LOADING_SELF_TABLE_BACKGROUND:String = "battleLoadingSelfTableBackground";

        public static const BATTLE_LOADING_SELF_TIPS_BACKGROUND:String = "battleLoadingSelfTipsBackground";

        public static const BLUE_ATSPG_H:String = "blue_atspg_h";

        public static const BLUE_ATSPG_M:String = "blue_atspg_m";

        public static const BLUE_SPG_H:String = "blue_spg_h";

        public static const BLUE_SPG_M:String = "blue_spg_m";

        public static const BLUE_TANK_SH:String = "blue_tank_sh";

        public static const BLUE_TANK_SH_DEAD:String = "blue_tank_sh_dead";

        public static const BOMBER_ENTRY:String = "BomberEntry";

        public static const BOOTCAMP_ENTRY:String = "BootcampEntry";

        public static const BTN_DISABLE_CLAIM:String = "btn_disable_claim";

        public static const BTN_DOWN_MOVE_TO_BLACKLIST:String = "btn_down_move_to_blacklist";

        public static const BTN_DOWN_UNDO_BLACKLIST:String = "btn_down_undo_blacklist";

        public static const BTN_HOVER_BLACKLIST:String = "btn_hover_blacklist";

        public static const BTN_HOVER_CLAIM:String = "btn_hover_claim";

        public static const BTN_HOVER_UNDO_BLACKLIST:String = "btn_hover_undo_blacklist";

        public static const BTN_NORMAL_BLACKLIST:String = "btn_normal_blacklist";

        public static const BTN_NORMAL_CLAIM:String = "btn_normal_claim";

        public static const BTN_NORMAL_UNDO_BLACKLIST:String = "btn_normal_undo_blacklist";

        public static const BUTTON_TOXIC_CHAT_OFF_DISABLEL:String = "button_toxic_chat_off_disablel";

        public static const BUTTON_TOXIC_CHAT_OFF_HOVER:String = "button_toxic_chat_off_hover";

        public static const BUTTON_TOXIC_CHAT_OFF_NORMAL:String = "button_toxic_chat_off_normal";

        public static const BUTTON_TOXIC_CHAT_ON_DISABLEL:String = "button_toxic_chat_on_disablel";

        public static const BUTTON_TOXIC_CHAT_ON_HOVER:String = "button_toxic_chat_on_hover";

        public static const BUTTON_TOXIC_CHAT_ON_NORMAL:String = "button_toxic_chat_on_normal";

        public static const CAMERA:String = "Camera";

        public static const CAMERA_WITH_DIRECTION:String = "CameraWithDirection";

        public static const CHASSIS:String = "chassis";

        public static const CHASSIS_DESTROYED:String = "chassis_destroyed";

        public static const CHASSIS_ORANGE:String = "chassis_orange";

        public static const CHASSIS_RED:String = "chassis_red";

        public static const CHINA_CH01_TYPE59:String = "china-Ch01_Type59";

        public static const CHINA_CH01_TYPE59_GOLD:String = "china-Ch01_Type59_Gold";

        public static const CHINA_CH02_TYPE62:String = "china-Ch02_Type62";

        public static const CHINA_CH03_WZ_111_A:String = "china-Ch03_WZ_111_A";

        public static const CHINA_CH03_WZ_111:String = "china-Ch03_WZ-111";

        public static const CHINA_CH04_T34_1:String = "china-Ch04_T34_1";

        public static const CHINA_CH04_T34_1_TRAINING:String = "china-Ch04_T34_1_training";

        public static const CHINA_CH05_T34_2:String = "china-Ch05_T34_2";

        public static const CHINA_CH06_RENAULT_NC31:String = "china-Ch06_Renault_NC31";

        public static const CHINA_CH06_RENAULT_NC31_BOT:String = "china-Ch06_Renault_NC31_bot";

        public static const CHINA_CH07_VICKERS_MK_E_TYPE_BT26:String = "china-Ch07_Vickers_MkE_Type_BT26";

        public static const CHINA_CH08_TYPE97_CHI_HA:String = "china-Ch08_Type97_Chi_Ha";

        public static const CHINA_CH08_TYPE97_CHI_HA_BOOTCAMP:String = "china-Ch08_Type97_Chi_Ha_bootcamp";

        public static const CHINA_CH09_M5:String = "china-Ch09_M5";

        public static const CHINA_CH10_IS2:String = "china-Ch10_IS2";

        public static const CHINA_CH11_110:String = "china-Ch11_110";

        public static const CHINA_CH11_110_IGR:String = "china-Ch11_110_IGR";

        public static const CHINA_CH12_111_1_2_3:String = "china-Ch12_111_1_2_3";

        public static const CHINA_CH14_T34_3:String = "china-Ch14_T34_3";

        public static const CHINA_CH15_59_16:String = "china-Ch15_59_16";

        public static const CHINA_CH16_WZ_131:String = "china-Ch16_WZ_131";

        public static const CHINA_CH17_WZ131_1_WZ132:String = "china-Ch17_WZ131_1_WZ132";

        public static const CHINA_CH17_WZ131_1_WZ132_IGR:String = "china-Ch17_WZ131_1_WZ132_IGR";

        public static const CHINA_CH18_WZ_120:String = "china-Ch18_WZ-120";

        public static const CHINA_CH19_121:String = "china-Ch19_121";

        public static const CHINA_CH19_121_IGR:String = "china-Ch19_121_IGR";

        public static const CHINA_CH20_TYPE58:String = "china-Ch20_Type58";

        public static const CHINA_CH21_T34:String = "china-Ch21_T34";

        public static const CHINA_CH22_113:String = "china-Ch22_113";

        public static const CHINA_CH22_113_BEIJING_OPERA:String = "china-Ch22_113_Beijing_Opera";

        public static const CHINA_CH22_113_P:String = "china-Ch22_113P";

        public static const CHINA_CH23_112:String = "china-Ch23_112";

        public static const CHINA_CH23_112_FL:String = "china-Ch23_112_FL";

        public static const CHINA_CH24_TYPE64:String = "china-Ch24_Type64";

        public static const CHINA_CH25_121_MOD_1971_B:String = "china-Ch25_121_mod_1971B";

        public static const CHINA_CH26_59_PATTON:String = "china-Ch26_59_Patton";

        public static const CHINA_CH28_WZ_132_A:String = "china-Ch28_WZ_132A";

        public static const CHINA_CH29_TYPE_62_C_PROT:String = "china-Ch29_Type_62C_prot";

        public static const CHINA_CH30_T_26_G_FT:String = "china-Ch30_T-26G_FT";

        public static const CHINA_CH31_M3_G_FT:String = "china-Ch31_M3G_FT";

        public static const CHINA_CH32_SU_76_G_FT:String = "china-Ch32_SU-76G_FT";

        public static const CHINA_CH33_60_G_FT:String = "china-Ch33_60G_FT";

        public static const CHINA_CH34_WZ131_G_FT:String = "china-Ch34_WZ131G_FT";

        public static const CHINA_CH35_T_34_2_G_FT:String = "china-Ch35_T-34-2G_FT";

        public static const CHINA_CH36_WZ111_1_G_FT:String = "china-Ch36_WZ111_1G_FT";

        public static const CHINA_CH37_WZ111_G_FT:String = "china-Ch37_WZ111G_FT";

        public static const CHINA_CH38_WZ113_G_FT:String = "china-Ch38_WZ113G_FT";

        public static const CHINA_CH39_WZ120_1_G_FT:String = "china-Ch39_WZ120_1G_FT";

        public static const CHINA_CH39_WZ120_1_G_FT_FL:String = "china-Ch39_WZ120_1G_FT_FL";

        public static const CHINA_CH40_WZ120_G_FT:String = "china-Ch40_WZ120G_FT";

        public static const CHINA_CH41_WZ_111_5_A:String = "china-Ch41_WZ_111_5A";

        public static const CHINA_CH41_WZ_111_5_A_BOB:String = "china-Ch41_WZ_111_5A_bob";

        public static const CHINA_CH41_WZ_111_QL:String = "china-Ch41_WZ_111_QL";

        public static const CHINA_CH42_WALKER_BULLDOG_M41_D:String = "china-Ch42_WalkerBulldog_M41D";

        public static const COMMANDER:String = "commander";

        public static const COMMANDER_ORANGE:String = "commander_orange";

        public static const CONTROL_POINT_ENTRY_0:String = "ControlPointEntry_0";

        public static const CONTROL_POINT_ENTRY_1:String = "ControlPointEntry_1";

        public static const CONTROL_POINT_ENTRY_2:String = "ControlPointEntry_2";

        public static const CONTROL_POINT_ENTRY_3:String = "ControlPointEntry_3";

        public static const CRITICAL:String = "critical";

        public static const CRITICAL_BASE:String = "critical_base";

        public static const CRITICAL_SIEGE:String = "critical_siege";

        public static const CRUISE_0:String = "Cruise_0";

        public static const CRUISE_1:String = "Cruise_1";

        public static const CRUISE_2:String = "Cruise_2";

        public static const CRUISE_3:String = "Cruise_3";

        public static const CRUISE_4:String = "Cruise_4";

        public static const CRUISE_5:String = "Cruise_5";

        public static const CZECH_CZ01_SKODA_T40:String = "czech-Cz01_Skoda_T40";

        public static const CZECH_CZ02_TVP_T50:String = "czech-Cz02_TVP_T50";

        public static const CZECH_CZ03_LT_VZ35:String = "czech-Cz03_LT_vz35";

        public static const CZECH_CZ04_T50_51:String = "czech-Cz04_T50_51";

        public static const CZECH_CZ05_T34_100:String = "czech-Cz05_T34_100";

        public static const CZECH_CZ06_KOLOHOUSENKA:String = "czech-Cz06_Kolohousenka";

        public static const CZECH_CZ07_TVP_46:String = "czech-Cz07_TVP_46";

        public static const CZECH_CZ08_T_25:String = "czech-Cz08_T_25";

        public static const CZECH_CZ09_T_24:String = "czech-Cz09_T_24";

        public static const CZECH_CZ10_LT_VZ38:String = "czech-Cz10_LT_vz38";

        public static const CZECH_CZ11_V_8_H:String = "czech-Cz11_V_8_H";

        public static const CZECH_CZ13_T_27:String = "czech-Cz13_T_27";

        public static const DAMAGE_BAR_GREEN:String = "damageBar_green";

        public static const DAMAGE_LOG_AIRSTRIKE_16X16:String = "damageLog_airstrike_16x16";

        public static const DAMAGE_LOG_ART_16X16:String = "damageLog_art_16x16";

        public static const DAMAGE_LOG_ARTILLERY_16X16:String = "damageLog_artillery_16x16";

        public static const DAMAGE_LOG_ASSIST_16X16:String = "damageLog_assist_16x16";

        public static const DAMAGE_LOG_COORDINATE_16X16:String = "damageLog_coordinate_16x16";

        public static const DAMAGE_LOG_CRITICAL_16X16:String = "damageLog_critical_16x16";

        public static const DAMAGE_LOG_CRITICAL_ENEMY_16X16:String = "damageLog_critical_enemy_16x16";

        public static const DAMAGE_LOG_CRITICAL_ENEMY_16X16_BLIND:String = "damageLog_critical_enemy_16x16Blind";

        public static const DAMAGE_LOG_DAMAGE_16X16:String = "damageLog_damage_16x16";

        public static const DAMAGE_LOG_DAMAGE_DETAIL:String = "damageLog_damage_detail";

        public static const DAMAGE_LOG_DAMAGE_DETAIL_TOP:String = "damageLog_damage_detailTop";

        public static const DAMAGE_LOG_DAMAGE_ENEMY_16X16:String = "damageLog_damage_enemy_16x16";

        public static const DAMAGE_LOG_DAMAGE_ENEMY_16X16_BLIND:String = "damageLog_damage_enemy_16x16Blind";

        public static const DAMAGE_LOG_DAMAGE_TOTAL:String = "damageLog_damage_total";

        public static const DAMAGE_LOG_DESTROYER_16X16:String = "damageLog_destroyer_16x16";

        public static const DAMAGE_LOG_DETAIL_NO_BORDER:String = "damageLog_detailNoBorder";

        public static const DAMAGE_LOG_DISCOVER_16X16:String = "damageLog_discover_16x16";

        public static const DAMAGE_LOG_FIRE_16X16:String = "damageLog_fire_16x16";

        public static const DAMAGE_LOG_FIRE_ENEMY_16X16:String = "damageLog_fire_enemy_16x16";

        public static const DAMAGE_LOG_FIRE_ENEMY_16X16_BLIND:String = "damageLog_fire_enemy_16x16Blind";

        public static const DAMAGE_LOG_HEAVY_16X16:String = "damageLog_heavy_16x16";

        public static const DAMAGE_LOG_IMMOBILIZED_16X16:String = "damageLog_immobilized_16x16";

        public static const DAMAGE_LOG_KILL_16X16:String = "damageLog_kill_16x16";

        public static const DAMAGE_LOG_LIGHT_16X16:String = "damageLog_light_16x16";

        public static const DAMAGE_LOG_MIDDLE_16X16:String = "damageLog_middle_16x16";

        public static const DAMAGE_LOG_RAM_16X16:String = "damageLog_ram_16x16";

        public static const DAMAGE_LOG_RAM_ENEMY_16X16:String = "damageLog_ram_enemy_16x16";

        public static const DAMAGE_LOG_RAM_ENEMY_16X16_BLIND:String = "damageLog_ram_enemy_16x16Blind";

        public static const DAMAGE_LOG_REFLECT_16X16:String = "damageLog_reflect_16x16";

        public static const DAMAGE_LOG_STUN_16X16:String = "damageLog_stun_16x16";

        public static const DAMAGE_PANEL_BG:String = "DamagePanel_bg";

        public static const DAMAGE_PANEL_BG_WHEELED:String = "DamagePanel_bg_wheeled";

        public static const DEAD_POINT_CAMERA_ENTRY:String = "DeadPointCameraEntry";

        public static const DEAD_POINT_ENTRY:String = "DeadPointEntry";

        public static const DESTROYED:String = "destroyed";

        public static const DESTROYED_BASE:String = "destroyed_base";

        public static const DESTROYED_SIEGE:String = "destroyed_siege";

        public static const DRIVER:String = "driver";

        public static const DRIVER_ORANGE:String = "driver_orange";

        public static const EMPTY_SHELL_BG:String = "emptyShellBg";

        public static const ENEMY_ATSPG_MS:String = "enemy_atspg-ms";

        public static const ENEMY_ATSPG_TS:String = "enemy_atspg-ts";

        public static const ENEMY_SPG_MS:String = "enemy_spg-ms";

        public static const ENEMY_SPG_TS:String = "enemy_spg-ts";

        public static const ENEMY_TANK_SH:String = "enemy_tank_sh";

        public static const ENEMY_TANK_SH_DEAD:String = "enemy_tank_sh_dead";

        public static const ENEMY_ABSORPTION_FLAG_ENTRY:String = "EnemyAbsorptionFlagEntry";

        public static const ENEMY_REPAIR_ACTIVE_ENTRY:String = "EnemyRepairActiveEntry";

        public static const ENEMY_REPAIR_ACTIVE_PURPLE_ENTRY:String = "EnemyRepairActivePurpleEntry";

        public static const ENEMY_REPAIR_COOLDOWN_ENTRY:String = "EnemyRepairCooldownEntry";

        public static const ENEMY_REPAIR_COOLDOWN_PURPLE_ENTRY:String = "EnemyRepairCooldownPurpleEntry";

        public static const ENEMY_TEAM_BASE_ENTRY_PURPLE_0:String = "EnemyTeamBaseEntry_purple_0";

        public static const ENEMY_TEAM_BASE_ENTRY_PURPLE_1:String = "EnemyTeamBaseEntry_purple_1";

        public static const ENEMY_TEAM_BASE_ENTRY_PURPLE_2:String = "EnemyTeamBaseEntry_purple_2";

        public static const ENEMY_TEAM_BASE_ENTRY_PURPLE_3:String = "EnemyTeamBaseEntry_purple_3";

        public static const ENEMY_TEAM_BASE_ENTRY_RED_0:String = "EnemyTeamBaseEntry_red_0";

        public static const ENEMY_TEAM_BASE_ENTRY_RED_1:String = "EnemyTeamBaseEntry_red_1";

        public static const ENEMY_TEAM_BASE_ENTRY_RED_2:String = "EnemyTeamBaseEntry_red_2";

        public static const ENEMY_TEAM_BASE_ENTRY_RED_3:String = "EnemyTeamBaseEntry_red_3";

        public static const ENEMY_TEAM_SPAWN_ENTRY_PURPLE_1:String = "EnemyTeamSpawnEntry_purple_1";

        public static const ENEMY_TEAM_SPAWN_ENTRY_PURPLE_2:String = "EnemyTeamSpawnEntry_purple_2";

        public static const ENEMY_TEAM_SPAWN_ENTRY_PURPLE_3:String = "EnemyTeamSpawnEntry_purple_3";

        public static const ENEMY_TEAM_SPAWN_ENTRY_PURPLE_4:String = "EnemyTeamSpawnEntry_purple_4";

        public static const ENEMY_TEAM_SPAWN_ENTRY_RED_1:String = "EnemyTeamSpawnEntry_red_1";

        public static const ENEMY_TEAM_SPAWN_ENTRY_RED_2:String = "EnemyTeamSpawnEntry_red_2";

        public static const ENEMY_TEAM_SPAWN_ENTRY_RED_3:String = "EnemyTeamSpawnEntry_red_3";

        public static const ENEMY_TEAM_SPAWN_ENTRY_RED_4:String = "EnemyTeamSpawnEntry_red_4";

        public static const ENGINE:String = "engine";

        public static const ENGINE_CRITICAL:String = "engine_critical";

        public static const ENGINE_DESTROYED:String = "engine_destroyed";

        public static const ENGINE_ORANGE:String = "engine_orange";

        public static const ENGINE_RED:String = "engine_red";

        public static const EPIC_BASE_CAP_MINIMAP_ENTRY_BACKGROUND:String = "epicBaseCapMinimapEntryBackground";

        public static const EPIC_RANDOM_BATTLE_LOADING_SELF_TABLE_BACKGROUND:String = "epicRandomBattleLoadingSelfTableBackground";

        public static const EPIC_RANDOM_BATTLE_LOADING_SELF_TIPS_BACKGROUND:String = "epicRandomBattleLoadingSelfTipsBackground";

        public static const EPIC_RANDOMBETA_ICON:String = "epicRandombeta_icon";

        public static const EPIC_RANDOMEPIC_LOADING_DEAD_BG:String = "epicRandomepicLoading_deadBg";

        public static const EPIC_RANDOM_SWITCH_BT_3COLS:String = "epicRandomSwitchBt_3cols";

        public static const EPIC_RANDOM_SWITCH_BT_HIDDEN:String = "epicRandomSwitchBt_hidden";

        public static const EPIC_RANDOM_SWITCH_BT_PLAYERNAMES:String = "epicRandomSwitchBt_playernames";

        public static const EPIC_RANDOM_SWITCH_BT_SHORT:String = "epicRandomSwitchBt_short";

        public static const EPIC_RANDOM_SWITCH_BT_TANKS:String = "epicRandomSwitchBt_tanks";

        public static const EPIC_STATS_DEAD_BG:String = "epicStats_deadBg";

        public static const EPIC_STATS_SELF_BG:String = "epicStats_selfBg";

        public static const FALLOUT_SCORE_PANEL_DIVIDER:String = "falloutScorePanelDivider";

        public static const FC_FULL_STATS_DEAD_BG:String = "FC_fullStats_deadBg";

        public static const FC_FULL_STATS_SELF_BG:String = "FC_fullStats_selfBg";

        public static const FLAG_COOLDOWN_MINIMAP_ENTRY:String = "FlagCooldownMinimapEntry";

        public static const FLAG_NEUTRAL_MINIMAP_ENTRY:String = "FlagNeutralMinimapEntry";

        public static const FLAG_PURPLE_MINIMAP_ENTRY:String = "FlagPurpleMinimapEntry";

        public static const FM_FULL_STATS_SELF_BG:String = "FM_fullStats_selfBg";

        public static const FM_FULL_STATS_TABLE_ITEM_DEAD_BG:String = "FM_fullStatsTableItem_deadBg";

        public static const FM_FULL_STATS_TEAM_BG:String = "FM_fullStatsTeam_bg";

        public static const FM_FULL_STATS_TEAM_CURRENT_PLAYER_BLIND:String = "FM_fullStatsTeam_currentPlayerBlind";

        public static const FM_FULL_STATS_TEAM_CURRENT_PLAYER_ICON:String = "FM_fullStatsTeam_currentPlayerIcon";

        public static const FM_FULL_STATS_TEAM_CURRENT_SQUAD_ICON:String = "FM_fullStatsTeam_currentSquadIcon";

        public static const FM_FULL_STATS_TEAM_CURRENT_SQUAD_ICON_BLIND:String = "FM_fullStatsTeam_currentSquadIconBlind";

        public static const FM_FULL_STATS_TEAM_PLAYER_ICON:String = "FM_fullStatsTeam_playerIcon";

        public static const FM_FULL_STATS_TEAM_SQUAD_ICON:String = "FM_fullStatsTeam_squadIcon";

        public static const FRAG_CORELATION_BG:String = "fragCorelation_bg";

        public static const FRANCE_F01_RENAULT_FT:String = "france-F01_RenaultFT";

        public static const FRANCE_F01_RENAULT_FT_BOT:String = "france-F01_RenaultFT_bot";

        public static const FRANCE_F02_D1:String = "france-F02_D1";

        public static const FRANCE_F03_D2:String = "france-F03_D2";

        public static const FRANCE_F04_B1:String = "france-F04_B1";

        public static const FRANCE_F04_B1_BOOTCAMP:String = "france-F04_B1_bootcamp";

        public static const FRANCE_F05_BDR_G1_B:String = "france-F05_BDR_G1B";

        public static const FRANCE_F06_ARL_44:String = "france-F06_ARL_44";

        public static const FRANCE_F07_AMX_M4_1945:String = "france-F07_AMX_M4_1945";

        public static const FRANCE_F08_AMX_50_100:String = "france-F08_AMX_50_100";

        public static const FRANCE_F08_AMX_50_100_IGR:String = "france-F08_AMX_50_100_IGR";

        public static const FRANCE_F09_AMX_50_120:String = "france-F09_AMX_50_120";

        public static const FRANCE_F10_AMX_50_B:String = "france-F10_AMX_50B";

        public static const FRANCE_F10_AMX_50_B_FALLOUT:String = "france-F10_AMX_50B_fallout";

        public static const FRANCE_F100_PANHARD_EBR_90:String = "france-F100_Panhard_EBR_90";

        public static const FRANCE_F106_PANHARD_EBR_75_MLE1954:String = "france-F106_Panhard_EBR_75_Mle1954";

        public static const FRANCE_F107_HOTCHKISS_EBR:String = "france-F107_Hotchkiss_EBR";

        public static const FRANCE_F108_PANHARD_EBR_105:String = "france-F108_Panhard_EBR_105";

        public static const FRANCE_F109_AMD_PANHARD_178_B:String = "france-F109_AMD_Panhard_178B";

        public static const FRANCE_F11_RENAULT_G1_R:String = "france-F11_Renault_G1R";

        public static const FRANCE_F110_LYNX_6X6:String = "france-F110_Lynx_6x6";

        public static const FRANCE_F111_AM39_GENDRON_SOMUA:String = "france-F111_AM39_Gendron_Somua";

        public static const FRANCE_F112_M10_RBFM:String = "france-F112_M10_RBFM";

        public static const FRANCE_F113_BRETAGNE_PANTHER:String = "france-F113_Bretagne_Panther";

        public static const FRANCE_F114_PROJET_4_1:String = "france-F114_Projet_4_1";

        public static const FRANCE_F115_LORRAINE_50T:String = "france-F115_Lorraine_50t";

        public static const FRANCE_F116_BAT_CHATILLON_BOURRASQUE:String = "france-F116_Bat_Chatillon_Bourrasque";

        public static const FRANCE_F12_HOTCHKISS_H35:String = "france-F12_Hotchkiss_H35";

        public static const FRANCE_F13_AMX38:String = "france-F13_AMX38";

        public static const FRANCE_F14_AMX40:String = "france-F14_AMX40";

        public static const FRANCE_F15_AMX_12T:String = "france-F15_AMX_12t";

        public static const FRANCE_F16_AMX_13_75:String = "france-F16_AMX_13_75";

        public static const FRANCE_F17_AMX_13_90:String = "france-F17_AMX_13_90";

        public static const FRANCE_F17_AMX_13_90_IGR:String = "france-F17_AMX_13_90_IGR";

        public static const FRANCE_F18_BAT_CHATILLON25T:String = "france-F18_Bat_Chatillon25t";

        public static const FRANCE_F18_BAT_CHATILLON25T_IGR:String = "france-F18_Bat_Chatillon25t_IGR";

        public static const FRANCE_F19_LORRAINE40T:String = "france-F19_Lorraine40t";

        public static const FRANCE_F20_RENAULT_BS:String = "france-F20_RenaultBS";

        public static const FRANCE_F21_LORRAINE39_L_AM:String = "france-F21_Lorraine39_L_AM";

        public static const FRANCE_F22_AMX_105_AM:String = "france-F22_AMX_105AM";

        public static const FRANCE_F23_AMX_13_F3_AM:String = "france-F23_AMX_13F3AM";

        public static const FRANCE_F24_LORRAINE155_50:String = "france-F24_Lorraine155_50";

        public static const FRANCE_F25_LORRAINE155_51:String = "france-F25_Lorraine155_51";

        public static const FRANCE_F27_FCM_36_PAK40:String = "france-F27_FCM_36Pak40";

        public static const FRANCE_F28_105_LE_FH18_B2:String = "france-F28_105_leFH18B2";

        public static const FRANCE_F28_105_LE_FH18_B2_IGR:String = "france-F28_105_leFH18B2_IGR";

        public static const FRANCE_F30_RENAULT_FT_AC:String = "france-F30_RenaultFT_AC";

        public static const FRANCE_F32_SOMUA_SAU_40:String = "france-F32_Somua_Sau_40";

        public static const FRANCE_F33_S_35_CA:String = "france-F33_S_35CA";

        public static const FRANCE_F33_S_35_CA_IGR:String = "france-F33_S_35CA_IGR";

        public static const FRANCE_F34_ARL_V39:String = "france-F34_ARL_V39";

        public static const FRANCE_F35_AMX_AC_MLE1946:String = "france-F35_AMX_AC_Mle1946";

        public static const FRANCE_F36_AMX_AC_MLE1948:String = "france-F36_AMX_AC_Mle1948";

        public static const FRANCE_F36_AMX_AC_MLE1948_IGR:String = "france-F36_AMX_AC_Mle1948_IGR";

        public static const FRANCE_F37_AMX50_FOCH:String = "france-F37_AMX50_Foch";

        public static const FRANCE_F38_BAT_CHATILLON155_58:String = "france-F38_Bat_Chatillon155_58";

        public static const FRANCE_F42_AMR_35:String = "france-F42_AMR_35";

        public static const FRANCE_F44_SOMUA_S35:String = "france-F44_Somua_S35";

        public static const FRANCE_F49_RENAULT_R35:String = "france-F49_RenaultR35";

        public static const FRANCE_F50_FCM36_20T:String = "france-F50_FCM36_20t";

        public static const FRANCE_F52_RENAULT_UE57:String = "france-F52_RenaultUE57";

        public static const FRANCE_F62_ELC_AMX:String = "france-F62_ELC_AMX";

        public static const FRANCE_F62_ELC_AMX_IGR:String = "france-F62_ELC_AMX_IGR";

        public static const FRANCE_F64_AMX_50_FOSH_155:String = "france-F64_AMX_50Fosh_155";

        public static const FRANCE_F64_AMX_50_FOSH_B:String = "france-F64_AMX_50Fosh_B";

        public static const FRANCE_F65_FCM_50T:String = "france-F65_FCM_50t";

        public static const FRANCE_F66_AMX_OB_AM105:String = "france-F66_AMX_Ob_Am105";

        public static const FRANCE_F67_BAT_CHATILLON155_55:String = "france-F67_Bat_Chatillon155_55";

        public static const FRANCE_F68_AMX_CHASSEUR_DE_CHAR_46:String = "france-F68_AMX_Chasseur_de_char_46";

        public static const FRANCE_F69_AMX13_57_100:String = "france-F69_AMX13_57_100";

        public static const FRANCE_F69_AMX13_57_100_GRAND_FINAL:String = "france-F69_AMX13_57_100_GrandFinal";

        public static const FRANCE_F70_SARL42:String = "france-F70_SARL42";

        public static const FRANCE_F71_AMX_30_PROTOTYPE:String = "france-F71_AMX_30_prototype";

        public static const FRANCE_F72_AMX_30:String = "france-F72_AMX_30";

        public static const FRANCE_F73_M4_A1_REVALORISE:String = "france-F73_M4A1_Revalorise";

        public static const FRANCE_F73_M4_A1_REVALORISE_FL:String = "france-F73_M4A1_Revalorise_FL";

        public static const FRANCE_F74_AMX_M4_1949:String = "france-F74_AMX_M4_1949";

        public static const FRANCE_F74_AMX_M4_1949_FL:String = "france-F74_AMX_M4_1949_FL";

        public static const FRANCE_F74_AMX_M4_1949_LIBERTE:String = "france-F74_AMX_M4_1949_Liberte";

        public static const FRANCE_F75_CHAR_DE_25T:String = "france-F75_Char_de_25t";

        public static const FRANCE_F81_CHAR_DE_65T:String = "france-F81_Char_de_65t";

        public static const FRANCE_F82_AMX_M4_MLE1949_TER:String = "france-F82_AMX_M4_Mle1949_Ter";

        public static const FRANCE_F83_AMX_M4_MLE1949_BIS:String = "france-F83_AMX_M4_Mle1949_Bis";

        public static const FRANCE_F84_SOMUA_SM:String = "france-F84_Somua_SM";

        public static const FRANCE_F85_M4_A1_FL10:String = "france-F85_M4A1_FL10";

        public static const FRANCE_F87_BATIGNOLLES_CHATILLON_12T:String = "france-F87_Batignolles-Chatillon_12t";

        public static const FRANCE_F88_AMX_13_105:String = "france-F88_AMX_13_105";

        public static const FRANCE_F89_CANON_DASSAUT_DE_105:String = "france-F89_Canon_dassaut_de_105";

        public static const FRANCE_F97_ELC_EVEN_90:String = "france-F97_ELC_EVEN_90";

        public static const FRANCE_F97_ELC_EVEN_90_FL:String = "france-F97_ELC_EVEN_90_FL";

        public static const FUEL_TANK:String = "fuelTank";

        public static const FUEL_TANK_ORANGE:String = "fuelTank_orange";

        public static const FUEL_TANK_RED:String = "fuelTank_red";

        public static const FULL_STATS_DEAD_BG:String = "fullStats_deadBg";

        public static const FULL_STATS_SELF_BG:String = "fullStats_selfBg";

        public static const FULL_STATS_PLAYER_STATUS_IN_BATTLE:String = "fullStatsPlayerStatus_inBattle";

        public static const FULL_STATS_PLAYER_STATUS_KILLED:String = "fullStatsPlayerStatus_killed";

        public static const FULL_STATS_PLAYER_STATUS_OFFLINE:String = "fullStatsPlayerStatus_offline";

        public static const FULL_STATS_VEHICLE_TYPE_GREEN_AT_SPG:String = "fullStatsVehicleType_green_AT-SPG";

        public static const FULL_STATS_VEHICLE_TYPE_GREEN_HEAVY_TANK:String = "fullStatsVehicleType_green_heavyTank";

        public static const FULL_STATS_VEHICLE_TYPE_GREEN_LIGHT_TANK:String = "fullStatsVehicleType_green_lightTank";

        public static const FULL_STATS_VEHICLE_TYPE_GREEN_MEDIUM_TANK:String = "fullStatsVehicleType_green_mediumTank";

        public static const FULL_STATS_VEHICLE_TYPE_GREEN_SPG:String = "fullStatsVehicleType_green_SPG";

        public static const FULL_STATS_VEHICLE_TYPE_PURPLE_AT_SPG:String = "fullStatsVehicleType_purple_AT-SPG";

        public static const FULL_STATS_VEHICLE_TYPE_PURPLE_HEAVY_TANK:String = "fullStatsVehicleType_purple_heavyTank";

        public static const FULL_STATS_VEHICLE_TYPE_PURPLE_LIGHT_TANK:String = "fullStatsVehicleType_purple_lightTank";

        public static const FULL_STATS_VEHICLE_TYPE_PURPLE_MEDIUM_TANK:String = "fullStatsVehicleType_purple_mediumTank";

        public static const FULL_STATS_VEHICLE_TYPE_PURPLE_SPG:String = "fullStatsVehicleType_purple_SPG";

        public static const FULL_STATS_VEHICLE_TYPE_RED_AT_SPG:String = "fullStatsVehicleType_red_AT-SPG";

        public static const FULL_STATS_VEHICLE_TYPE_RED_HEAVY_TANK:String = "fullStatsVehicleType_red_heavyTank";

        public static const FULL_STATS_VEHICLE_TYPE_RED_LIGHT_TANK:String = "fullStatsVehicleType_red_lightTank";

        public static const FULL_STATS_VEHICLE_TYPE_RED_MEDIUM_TANK:String = "fullStatsVehicleType_red_mediumTank";

        public static const FULL_STATS_VEHICLE_TYPE_RED_SPG:String = "fullStatsVehicleType_red_SPG";

        public static const GERMANY_G02_HUMMEL:String = "germany-G02_Hummel";

        public static const GERMANY_G03_PZ_V_PANTHER:String = "germany-G03_PzV_Panther";

        public static const GERMANY_G03_PZ_V_PANTHER_BOOTCAMP:String = "germany-G03_PzV_Panther_bootcamp";

        public static const GERMANY_G03_PZ_V_PANTHER_IGR:String = "germany-G03_PzV_Panther_IGR";

        public static const GERMANY_G03_PZ_V_PANTHER_TRAINING:String = "germany-G03_PzV_Panther_training";

        public static const GERMANY_G04_PZ_VI_TIGER_I:String = "germany-G04_PzVI_Tiger_I";

        public static const GERMANY_G04_PZ_VI_TIGER_I_IGR:String = "germany-G04_PzVI_Tiger_I_IGR";

        public static const GERMANY_G04_PZ_VI_TIGER_IA:String = "germany-G04_PzVI_Tiger_IA";

        public static const GERMANY_G05_STU_G_40_AUSF_G:String = "germany-G05_StuG_40_AusfG";

        public static const GERMANY_G05_STU_G_40_AUSF_G_IGR:String = "germany-G05_StuG_40_AusfG_IGR";

        public static const GERMANY_G06_PZ_II:String = "germany-G06_PzII";

        public static const GERMANY_G06_PZ_II_BOT:String = "germany-G06_PzII_bot";

        public static const GERMANY_G07_PZ35T:String = "germany-G07_Pz35t";

        public static const GERMANY_G08_PZ38T:String = "germany-G08_Pz38t";

        public static const GERMANY_G08_PZ38T_BOOTCAMP:String = "germany-G08_Pz38t_bootcamp";

        public static const GERMANY_G09_HETZER:String = "germany-G09_Hetzer";

        public static const GERMANY_G10_PZ_III_AUSF_J:String = "germany-G10_PzIII_AusfJ";

        public static const GERMANY_G10_PZ_III_AUSF_J_TRAINING:String = "germany-G10_PzIII_AusfJ_training";

        public static const GERMANY_G100_GTRAKTOR_KRUPP:String = "germany-G100_Gtraktor_Krupp";

        public static const GERMANY_G101_STU_G_III:String = "germany-G101_StuG_III";

        public static const GERMANY_G102_PZ_III:String = "germany-G102_Pz_III";

        public static const GERMANY_G102_PZ_III_BOOTCAMP:String = "germany-G102_Pz_III_bootcamp";

        public static const GERMANY_G103_RU_251:String = "germany-G103_RU_251";

        public static const GERMANY_G104_STUG_IV:String = "germany-G104_Stug_IV";

        public static const GERMANY_G105_T_55_NVA_DDR:String = "germany-G105_T-55_NVA_DDR";

        public static const GERMANY_G106_PZ_KPFW_PANTHER_AUSF_F:String = "germany-G106_PzKpfwPanther_AusfF";

        public static const GERMANY_G107_PZ_KPFW_III_AUSF_K:String = "germany-G107_PzKpfwIII_AusfK";

        public static const GERMANY_G108_PZ_KPFW_II_AUSF_D:String = "germany-G108_PzKpfwII_AusfD";

        public static const GERMANY_G109_STEYR_WT:String = "germany-G109_Steyr_WT";

        public static const GERMANY_G11_BISON_I:String = "germany-G11_Bison_I";

        public static const GERMANY_G110_TYP_205:String = "germany-G110_Typ_205";

        public static const GERMANY_G112_KANONEN_JAGD_PANZER:String = "germany-G112_KanonenJagdPanzer";

        public static const GERMANY_G112_KANONEN_JAGD_PANZER_105:String = "germany-G112_KanonenJagdPanzer_105";

        public static const GERMANY_G113_SP_I_C:String = "germany-G113_SP_I_C";

        public static const GERMANY_G114_RHEINMETALL_SKORPIAN:String = "germany-G114_Rheinmetall_Skorpian";

        public static const GERMANY_G114_SKORPIAN:String = "germany-G114_Skorpian";

        public static const GERMANY_G115_TYP_205_4_JUN:String = "germany-G115_Typ_205_4_Jun";

        public static const GERMANY_G115_TYP_205_B:String = "germany-G115_Typ_205B";

        public static const GERMANY_G116_TURAN_III_PROT:String = "germany-G116_Turan_III_prot";

        public static const GERMANY_G117_TOLDI_III:String = "germany-G117_Toldi_III";

        public static const GERMANY_G117_TOLDI_III_BOOTCAMP:String = "germany-G117_Toldi_III_bootcamp";

        public static const GERMANY_G118_VK4503:String = "germany-G118_VK4503";

        public static const GERMANY_G119_PANZER58:String = "germany-G119_Panzer58";

        public static const GERMANY_G119_PANZER58_BF:String = "germany-G119_Panzer58_BF";

        public static const GERMANY_G119_PANZER58_FL:String = "germany-G119_Panzer58_FL";

        public static const GERMANY_G119_PZ58_MUTZ:String = "germany-G119_Pz58_Mutz";

        public static const GERMANY_G12_LTRAKTOR:String = "germany-G12_Ltraktor";

        public static const GERMANY_G12_LTRAKTOR_BOT:String = "germany-G12_Ltraktor_bot";

        public static const GERMANY_G120_M41_90:String = "germany-G120_M41_90";

        public static const GERMANY_G120_M41_90_GRAND_FINAL:String = "germany-G120_M41_90_GrandFinal";

        public static const GERMANY_G121_GRILLE_15_L63:String = "germany-G121_Grille_15_L63";

        public static const GERMANY_G122_VK6501_H:String = "germany-G122_VK6501H";

        public static const GERMANY_G125_SPZ_57_RH:String = "germany-G125_Spz_57_Rh";

        public static const GERMANY_G126_HWK_12:String = "germany-G126_HWK_12";

        public static const GERMANY_G13_VK3001_H:String = "germany-G13_VK3001H";

        public static const GERMANY_G134_PZ_KPFW_VII:String = "germany-G134_PzKpfw_VII";

        public static const GERMANY_G134_PZ_KPFW_VII_BOB:String = "germany-G134_PzKpfw_VII_bob";

        public static const GERMANY_G136_TIGER_131:String = "germany-G136_Tiger_131";

        public static const GERMANY_G137_PZ_VI_TIGER_217:String = "germany-G137_PzVI_Tiger_217";

        public static const GERMANY_G138_VK168_02:String = "germany-G138_VK168_02";

        public static const GERMANY_G138_VK168_02_MAUERBRECHER:String = "germany-G138_VK168_02_Mauerbrecher";

        public static const GERMANY_G139_MKA:String = "germany-G139_MKA";

        public static const GERMANY_G140_HWK_30:String = "germany-G140_HWK_30";

        public static const GERMANY_G141_VK7501_K:String = "germany-G141_VK7501K";

        public static const GERMANY_G142_M48_R_PZ:String = "germany-G142_M48RPz";

        public static const GERMANY_G143_E75_TS:String = "germany-G143_E75_TS";

        public static const GERMANY_G144_KPZ_50T:String = "germany-G144_Kpz_50t";

        public static const GERMANY_G15_VK3601_H:String = "germany-G15_VK3601H";

        public static const GERMANY_G16_PZ_VIB_TIGER_II:String = "germany-G16_PzVIB_Tiger_II";

        public static const GERMANY_G16_PZ_VIB_TIGER_II_F:String = "germany-G16_PzVIB_Tiger_II_F";

        public static const GERMANY_G16_PZ_VIB_TIGER_II_IGR:String = "germany-G16_PzVIB_Tiger_II_IGR";

        public static const GERMANY_G16_PZ_VIB_TIGER_II_TRAINING:String = "germany-G16_PzVIB_Tiger_II_training";

        public static const GERMANY_G17_JAGD_PZ_IV:String = "germany-G17_JagdPzIV";

        public static const GERMANY_G18_JAGD_PANTHER:String = "germany-G18_JagdPanther";

        public static const GERMANY_G19_WESPE:String = "germany-G19_Wespe";

        public static const GERMANY_G20_MARDER_II:String = "germany-G20_Marder_II";

        public static const GERMANY_G21_PANZER_JAGER_I:String = "germany-G21_PanzerJager_I";

        public static const GERMANY_G21_PANZER_JAGER_I_BOOTCAMP:String = "germany-G21_PanzerJager_I_bootcamp";

        public static const GERMANY_G22_STURMPANZER_II:String = "germany-G22_Sturmpanzer_II";

        public static const GERMANY_G23_GRILLE:String = "germany-G23_Grille";

        public static const GERMANY_G24_VK3002_DB:String = "germany-G24_VK3002DB";

        public static const GERMANY_G25_PZ_II_LUCHS:String = "germany-G25_PzII_Luchs";

        public static const GERMANY_G26_VK1602:String = "germany-G26_VK1602";

        public static const GERMANY_G27_VK3001_P:String = "germany-G27_VK3001P";

        public static const GERMANY_G28_PZ_III_IV:String = "germany-G28_PzIII_IV";

        public static const GERMANY_G32_PZ_V_PZ_IV:String = "germany-G32_PzV_PzIV";

        public static const GERMANY_G32_PZ_V_PZ_IV_AUSF_ALFA:String = "germany-G32_PzV_PzIV_ausf_Alfa";

        public static const GERMANY_G32_PZ_V_PZ_IV_AUSF_ALFA_CN:String = "germany-G32_PzV_PzIV_ausf_Alfa_CN";

        public static const GERMANY_G32_PZ_V_PZ_IV_CN:String = "germany-G32_PzV_PzIV_CN";

        public static const GERMANY_G33_H39_CAPTURED:String = "germany-G33_H39_captured";

        public static const GERMANY_G34_S35_CAPTURED:String = "germany-G34_S35_captured";

        public static const GERMANY_G35_B_1BIS_CAPTURED:String = "germany-G35_B-1bis_captured";

        public static const GERMANY_G36_PZ_II_J:String = "germany-G36_PzII_J";

        public static const GERMANY_G37_FERDINAND:String = "germany-G37_Ferdinand";

        public static const GERMANY_G37_FERDINAND_IGR:String = "germany-G37_Ferdinand_IGR";

        public static const GERMANY_G39_MARDER_III:String = "germany-G39_Marder_III";

        public static const GERMANY_G40_NASHORN:String = "germany-G40_Nashorn";

        public static const GERMANY_G41_DICKER_MAX:String = "germany-G41_DickerMax";

        public static const GERMANY_G42_MAUS:String = "germany-G42_Maus";

        public static const GERMANY_G42_MAUS_IGR:String = "germany-G42_Maus_IGR";

        public static const GERMANY_G43_STURER_EMIL:String = "germany-G43_Sturer_Emil";

        public static const GERMANY_G44_JAGD_TIGER:String = "germany-G44_JagdTiger";

        public static const GERMANY_G44_JAGD_TIGER_H:String = "germany-G44_JagdTigerH";

        public static const GERMANY_G45_G_TIGER:String = "germany-G45_G_Tiger";

        public static const GERMANY_G46_T_25:String = "germany-G46_T-25";

        public static const GERMANY_G48_E_25:String = "germany-G48_E-25";

        public static const GERMANY_G48_E_25_IGR:String = "germany-G48_E-25_IGR";

        public static const GERMANY_G49_G_PANTHER:String = "germany-G49_G_Panther";

        public static const GERMANY_G50_T_15:String = "germany-G50_T-15";

        public static const GERMANY_G51_LOWE:String = "germany-G51_Lowe";

        public static const GERMANY_G51_LOWE_FL:String = "germany-G51_Lowe_FL";

        public static const GERMANY_G52_PZ38_NA:String = "germany-G52_Pz38_NA";

        public static const GERMANY_G53_PZ_I:String = "germany-G53_PzI";

        public static const GERMANY_G54_E_50:String = "germany-G54_E-50";

        public static const GERMANY_G55_E_75:String = "germany-G55_E-75";

        public static const GERMANY_G56_E_100:String = "germany-G56_E-100";

        public static const GERMANY_G57_PZ_VI_TIGER_P:String = "germany-G57_PzVI_Tiger_P";

        public static const GERMANY_G58_VK4502_P:String = "germany-G58_VK4502P";

        public static const GERMANY_G58_VK4502_P7:String = "germany-G58_VK4502P7";

        public static const GERMANY_G61_G_E:String = "germany-G61_G_E";

        public static const GERMANY_G62_ROKET_STURMTIGER:String = "germany-G62_Roket_Sturmtiger";

        public static const GERMANY_G63_PZ_I_AUSF_C:String = "germany-G63_PzI_ausf_C";

        public static const GERMANY_G64_PANTHER_II:String = "germany-G64_Panther_II";

        public static const GERMANY_G64_PANTHER_II_IGR:String = "germany-G64_Panther_II_IGR";

        public static const GERMANY_G65_JAGD_TIGER_SD_KFZ_185:String = "germany-G65_JagdTiger_SdKfz_185";

        public static const GERMANY_G65_JAGD_TIGER_SD_KFZ_185_IGR:String = "germany-G65_JagdTiger_SdKfz_185_IGR";

        public static const GERMANY_G66_VK2801:String = "germany-G66_VK2801";

        public static const GERMANY_G67_VK4502_A:String = "germany-G67_VK4502A";

        public static const GERMANY_G70_PZ_IV_HYDRO:String = "germany-G70_PzIV_Hydro";

        public static const GERMANY_G71_JAGD_PANTHER_II:String = "germany-G71_JagdPantherII";

        public static const GERMANY_G72_JAGD_PZ_E100:String = "germany-G72_JagdPz_E100";

        public static const GERMANY_G73_E50_AUSF_M:String = "germany-G73_E50_Ausf_M";

        public static const GERMANY_G76_PZ_SFL_I_VC:String = "germany-G76_Pz_Sfl_IVc";

        public static const GERMANY_G77_PZ_IV_SCHMALTURM:String = "germany-G77_PzIV_Schmalturm";

        public static const GERMANY_G78_PANTHER_M10:String = "germany-G78_Panther_M10";

        public static const GERMANY_G79_PZ_IV_AUSF_GH:String = "germany-G79_Pz_IV_AusfGH";

        public static const GERMANY_G80_PZ_IV_AUSF_D:String = "germany-G80_Pz_IV_AusfD";

        public static const GERMANY_G80_PZ_IV_AUSF_D_BOOTCAMP:String = "germany-G80_Pz_IV_AusfD_bootcamp";

        public static const GERMANY_G81_PZ_IV_AUSF_H:String = "germany-G81_Pz_IV_AusfH";

        public static const GERMANY_G82_PZ_II_AUSF_G:String = "germany-G82_Pz_II_AusfG";

        public static const GERMANY_G83_PZ_IV_AUSF_A:String = "germany-G83_Pz_IV_AusfA";

        public static const GERMANY_G85_AUF_PANTHER:String = "germany-G85_Auf_Panther";

        public static const GERMANY_G85_AUFKLARUNGSPANZER_V:String = "germany-G85_Aufklarungspanzer_V";

        public static const GERMANY_G86_VK2001_DB:String = "germany-G86_VK2001DB";

        public static const GERMANY_G87_VK3002_DB_V1:String = "germany-G87_VK3002DB_V1";

        public static const GERMANY_G88_INDIEN_PANZER:String = "germany-G88_Indien_Panzer";

        public static const GERMANY_G89_LEOPARD1:String = "germany-G89_Leopard1";

        public static const GERMANY_G90_DW_II:String = "germany-G90_DW_II";

        public static const GERMANY_G91_PRO_AG_A:String = "germany-G91_Pro_Ag_A";

        public static const GERMANY_G92_VK7201:String = "germany-G92_VK7201";

        public static const GERMANY_G93_GW_MK_V_IE:String = "germany-G93_GW_Mk_VIe";

        public static const GERMANY_G94_GW_TIGER_P:String = "germany-G94_GW_Tiger_P";

        public static const GERMANY_G95_PZ_SFL_I_VB:String = "germany-G95_Pz_Sfl_IVb";

        public static const GERMANY_G96_VK3002_M:String = "germany-G96_VK3002M";

        public static const GERMANY_G97_WAFFENTRAGER_IV:String = "germany-G97_Waffentrager_IV";

        public static const GERMANY_G98_WAFFENTRAGER_E100:String = "germany-G98_Waffentrager_E100";

        public static const GERMANY_G98_WAFFENTRAGER_E100_P:String = "germany-G98_Waffentrager_E100_P";

        public static const GERMANY_G99_RH_B_WAFFENTRAGER:String = "germany-G99_RhB_Waffentrager";

        public static const GOLD_ATSPG_HS:String = "gold_atspg_hs";

        public static const GOLD_ATSPG_MS:String = "gold_atspg_ms";

        public static const GOLD_SPG_HS:String = "gold_spg_hs";

        public static const GOLD_SPG_MS:String = "gold_spg_ms";

        public static const GOLD_TANK_SH:String = "gold_tank_sh";

        public static const GOLD_TANK_SH_DEAD:String = "gold_tank_sh_dead";

        public static const GOLD_CENTER:String = "goldCenter";

        public static const GOLD_EDGE:String = "goldEdge";

        public static const GREEN_AT_SPG_DESTROYED:String = "greenAT-SPGDestroyed";

        public static const GREEN_AT_SPG_NORMAL:String = "greenAT-SPGNormal";

        public static const GREEN_FALLOUT_SCORE_PANEL_ARROW:String = "greenFalloutScorePanelArrow";

        public static const GREENHEAVY_TANK_DESTROYED:String = "greenheavyTankDestroyed";

        public static const GREENHEAVY_TANK_NORMAL:String = "greenheavyTankNormal";

        public static const GREENLIGHT_TANK_DESTROYED:String = "greenlightTankDestroyed";

        public static const GREENLIGHT_TANK_NORMAL:String = "greenlightTankNormal";

        public static const GREENMEDIUM_TANK_DESTROYED:String = "greenmediumTankDestroyed";

        public static const GREENMEDIUM_TANK_NORMAL:String = "greenmediumTankNormal";

        public static const GREEN_SPG_DESTROYED:String = "greenSPGDestroyed";

        public static const GREEN_SPG_NORMAL:String = "greenSPGNormal";

        public static const GUN:String = "gun";

        public static const GUN_ORANGE:String = "gun_orange";

        public static const GUN_RED:String = "gun_red";

        public static const GUNNER:String = "gunner";

        public static const GUNNER_ORANGE:String = "gunner_orange";

        public static const HEADQUARTER_MINIMAP_ENTRY_ALLY:String = "HeadquarterMinimapEntryAlly";

        public static const HEADQUARTER_MINIMAP_ENTRY_ALLY_DESTROYED:String = "HeadquarterMinimapEntryAllyDestroyed";

        public static const HEADQUARTER_MINIMAP_ENTRY_ENEMY:String = "HeadquarterMinimapEntryEnemy";

        public static const HEADQUARTER_MINIMAP_ENTRY_ENEMY_DESTROYED:String = "HeadquarterMinimapEntryEnemyDestroyed";

        public static const HEAVY_TANK_ALLY_DEAD_GREEN:String = "heavyTank_ally_dead_green";

        public static const HEAVY_TANK_ALLY_GREEN:String = "heavyTank_ally_green";

        public static const HEAVY_TANK_ENEMY_DEAD_PURPLE:String = "heavyTank_enemy_dead_purple";

        public static const HEAVY_TANK_ENEMY_DEAD_RED:String = "heavyTank_enemy_dead_red";

        public static const HEAVY_TANK_ENEMY_PURPLE:String = "heavyTank_enemy_purple";

        public static const HEAVY_TANK_ENEMY_RED:String = "heavyTank_enemy_red";

        public static const HEAVY_TANK_SQUADMAN_DEAD_GOLD:String = "heavyTank_squadman_dead_gold";

        public static const HEAVY_TANK_SQUADMAN_DEAD_YELLOW:String = "heavyTank_squadman_dead_yellow";

        public static const HEAVY_TANK_SQUADMAN_GOLD:String = "heavyTank_squadman_gold";

        public static const HEAVY_TANK_SQUADMAN_YELLOW:String = "heavyTank_squadman_yellow";

        public static const HEAVY_TANK_TEAM_KILLER_BLUE:String = "heavyTank_teamKiller_blue";

        public static const HEAVY_TANK_TEAM_KILLER_DEAD_BLUE:String = "heavyTank_teamKiller_dead_blue";

        public static const HELP_WINDOW_BG:String = "helpWindow_bg";

        public static const HELP_WINDOW_BOTTOM_BG:String = "helpWindow_bottom_bg";

        public static const HELP_WINDOW_INFO:String = "helpWindow_info";

        public static const ICO_IGR:String = "icoIGR";

        public static const ICON_PLATOON:String = "icon_platoon";

        public static const ICON_RANK:String = "icon_rank";

        public static const ICON_RANK_0_0_24X24:String = "icon_rank_0_0_24x24";

        public static const ICON_RANK_0_1_24X24:String = "icon_rank_0_1_24x24";

        public static const ICON_RANK_0_10_24X24:String = "icon_rank_0_10_24x24";

        public static const ICON_RANK_0_11_24X24:String = "icon_rank_0_11_24x24";

        public static const ICON_RANK_0_12_24X24:String = "icon_rank_0_12_24x24";

        public static const ICON_RANK_0_13_24X24:String = "icon_rank_0_13_24x24";

        public static const ICON_RANK_0_14_24X24:String = "icon_rank_0_14_24x24";

        public static const ICON_RANK_0_15_24X24:String = "icon_rank_0_15_24x24";

        public static const ICON_RANK_0_2_24X24:String = "icon_rank_0_2_24x24";

        public static const ICON_RANK_0_3_24X24:String = "icon_rank_0_3_24x24";

        public static const ICON_RANK_0_4_24X24:String = "icon_rank_0_4_24x24";

        public static const ICON_RANK_0_5_24X24:String = "icon_rank_0_5_24x24";

        public static const ICON_RANK_0_6_24X24:String = "icon_rank_0_6_24x24";

        public static const ICON_RANK_0_7_24X24:String = "icon_rank_0_7_24x24";

        public static const ICON_RANK_0_8_24X24:String = "icon_rank_0_8_24x24";

        public static const ICON_RANK_0_9_24X24:String = "icon_rank_0_9_24x24";

        public static const ICON_RANK_1_0_24X24:String = "icon_rank_1_0_24x24";

        public static const ICON_RANK_1_1_24X24:String = "icon_rank_1_1_24x24";

        public static const ICON_RANK_1_10_24X24:String = "icon_rank_1_10_24x24";

        public static const ICON_RANK_1_11_24X24:String = "icon_rank_1_11_24x24";

        public static const ICON_RANK_1_12_24X24:String = "icon_rank_1_12_24x24";

        public static const ICON_RANK_1_13_24X24:String = "icon_rank_1_13_24x24";

        public static const ICON_RANK_1_14_24X24:String = "icon_rank_1_14_24x24";

        public static const ICON_RANK_1_15_24X24:String = "icon_rank_1_15_24x24";

        public static const ICON_RANK_1_2_24X24:String = "icon_rank_1_2_24x24";

        public static const ICON_RANK_1_3_24X24:String = "icon_rank_1_3_24x24";

        public static const ICON_RANK_1_4_24X24:String = "icon_rank_1_4_24x24";

        public static const ICON_RANK_1_5_24X24:String = "icon_rank_1_5_24x24";

        public static const ICON_RANK_1_6_24X24:String = "icon_rank_1_6_24x24";

        public static const ICON_RANK_1_7_24X24:String = "icon_rank_1_7_24x24";

        public static const ICON_RANK_1_8_24X24:String = "icon_rank_1_8_24x24";

        public static const ICON_RANK_1_9_24X24:String = "icon_rank_1_9_24x24";

        public static const ICON_RANK_2_0_24X24:String = "icon_rank_2_0_24x24";

        public static const ICON_RANK_2_1_24X24:String = "icon_rank_2_1_24x24";

        public static const ICON_RANK_2_10_24X24:String = "icon_rank_2_10_24x24";

        public static const ICON_RANK_2_11_24X24:String = "icon_rank_2_11_24x24";

        public static const ICON_RANK_2_12_24X24:String = "icon_rank_2_12_24x24";

        public static const ICON_RANK_2_13_24X24:String = "icon_rank_2_13_24x24";

        public static const ICON_RANK_2_14_24X24:String = "icon_rank_2_14_24x24";

        public static const ICON_RANK_2_15_24X24:String = "icon_rank_2_15_24x24";

        public static const ICON_RANK_2_2_24X24:String = "icon_rank_2_2_24x24";

        public static const ICON_RANK_2_3_24X24:String = "icon_rank_2_3_24x24";

        public static const ICON_RANK_2_4_24X24:String = "icon_rank_2_4_24x24";

        public static const ICON_RANK_2_5_24X24:String = "icon_rank_2_5_24x24";

        public static const ICON_RANK_2_6_24X24:String = "icon_rank_2_6_24x24";

        public static const ICON_RANK_2_7_24X24:String = "icon_rank_2_7_24x24";

        public static const ICON_RANK_2_8_24X24:String = "icon_rank_2_8_24x24";

        public static const ICON_RANK_2_9_24X24:String = "icon_rank_2_9_24x24";

        public static const ICON_RANK_3_0_24X24:String = "icon_rank_3_0_24x24";

        public static const ICON_RANK_3_1_24X24:String = "icon_rank_3_1_24x24";

        public static const ICON_RANK_3_10_24X24:String = "icon_rank_3_10_24x24";

        public static const ICON_RANK_3_11_24X24:String = "icon_rank_3_11_24x24";

        public static const ICON_RANK_3_12_24X24:String = "icon_rank_3_12_24x24";

        public static const ICON_RANK_3_13_24X24:String = "icon_rank_3_13_24x24";

        public static const ICON_RANK_3_14_24X24:String = "icon_rank_3_14_24x24";

        public static const ICON_RANK_3_15_24X24:String = "icon_rank_3_15_24x24";

        public static const ICON_RANK_3_2_24X24:String = "icon_rank_3_2_24x24";

        public static const ICON_RANK_3_3_24X24:String = "icon_rank_3_3_24x24";

        public static const ICON_RANK_3_4_24X24:String = "icon_rank_3_4_24x24";

        public static const ICON_RANK_3_5_24X24:String = "icon_rank_3_5_24x24";

        public static const ICON_RANK_3_6_24X24:String = "icon_rank_3_6_24x24";

        public static const ICON_RANK_3_7_24X24:String = "icon_rank_3_7_24x24";

        public static const ICON_RANK_3_8_24X24:String = "icon_rank_3_8_24x24";

        public static const ICON_RANK_3_9_24X24:String = "icon_rank_3_9_24x24";

        public static const ICON_RANK_4_0_24X24:String = "icon_rank_4_0_24x24";

        public static const ICON_RANKS_GROUP_0_1_24X24:String = "icon_ranks_group_0_1_24x24";

        public static const ICON_RANKS_GROUP_0_2_24X24:String = "icon_ranks_group_0_2_24x24";

        public static const ICON_RANKS_GROUP_0_3_24X24:String = "icon_ranks_group_0_3_24x24";

        public static const ICON_RANKS_GROUP_1_1_24X24:String = "icon_ranks_group_1_1_24x24";

        public static const ICON_RANKS_GROUP_1_2_24X24:String = "icon_ranks_group_1_2_24x24";

        public static const ICON_RANKS_GROUP_1_3_24X24:String = "icon_ranks_group_1_3_24x24";

        public static const ICON_RANKS_GROUP_2_1_24X24:String = "icon_ranks_group_2_1_24x24";

        public static const ICON_RANKS_GROUP_2_2_24X24:String = "icon_ranks_group_2_2_24x24";

        public static const ICON_RANKS_GROUP_2_3_24X24:String = "icon_ranks_group_2_3_24x24";

        public static const ICON_RANKS_GROUP_3_1_24X24:String = "icon_ranks_group_3_1_24x24";

        public static const ICON_RANKS_GROUP_3_2_24X24:String = "icon_ranks_group_3_2_24x24";

        public static const ICON_RANKS_GROUP_3_3_24X24:String = "icon_ranks_group_3_3_24x24";

        public static const ICON_TANK:String = "icon_tank";

        public static const ICON_TOXIC_CHAT_OFF:String = "icon_toxic_chat_off";

        public static const ITALY_IT03_M15_42:String = "italy-It03_M15_42";

        public static const ITALY_IT04_FIAT_3000:String = "italy-It04_Fiat_3000";

        public static const ITALY_IT05_CARRO_L6_40:String = "italy-It05_Carro_L6_40";

        public static const ITALY_IT06_M14_41:String = "italy-It06_M14_41";

        public static const ITALY_IT07_P26_40:String = "italy-It07_P26_40";

        public static const ITALY_IT08_PROGETTO_M40_MOD_65:String = "italy-It08_Progetto_M40_mod_65";

        public static const ITALY_IT08_PROGETTO_M40_MOD_65_BOB:String = "italy-It08_Progetto_M40_mod_65_bob";

        public static const ITALY_IT09_P43_TER:String = "italy-It09_P43_ter";

        public static const ITALY_IT10_P43_BIS:String = "italy-It10_P43_bis";

        public static const ITALY_IT11_P43:String = "italy-It11_P43";

        public static const ITALY_IT12_PROTOTIPO_STANDARD_B:String = "italy-It12_Prototipo_Standard_B";

        public static const ITALY_IT13_PROGETTO_M35_MOD_46:String = "italy-It13_Progetto_M35_mod_46";

        public static const ITALY_IT14_P44_PANTERA:String = "italy-It14_P44_Pantera";

        public static const JAPAN_J01_NC27:String = "japan-J01_NC27";

        public static const JAPAN_J01_NC27_BOT:String = "japan-J01_NC27_bot";

        public static const JAPAN_J02_TE_KE:String = "japan-J02_Te_Ke";

        public static const JAPAN_J03_HA_GO:String = "japan-J03_Ha_Go";

        public static const JAPAN_J04_KE_NI:String = "japan-J04_Ke_Ni";

        public static const JAPAN_J04_KE_NI_BOOTCAMP:String = "japan-J04_Ke_Ni_bootcamp";

        public static const JAPAN_J05_KE_NI_B:String = "japan-J05_Ke_Ni_B";

        public static const JAPAN_J06_KE_HO:String = "japan-J06_Ke_Ho";

        public static const JAPAN_J07_CHI_HA:String = "japan-J07_Chi_Ha";

        public static const JAPAN_J08_CHI_NU:String = "japan-J08_Chi_Nu";

        public static const JAPAN_J09_CHI_HE:String = "japan-J09_Chi_He";

        public static const JAPAN_J10_CHI_TO:String = "japan-J10_Chi_To";

        public static const JAPAN_J11_CHI_RI:String = "japan-J11_Chi_Ri";

        public static const JAPAN_J11_CHI_RI_IGR:String = "japan-J11_Chi_Ri_IGR";

        public static const JAPAN_J12_CHI_NU_KAI:String = "japan-J12_Chi_Nu_Kai";

        public static const JAPAN_J13_STA_1:String = "japan-J13_STA_1";

        public static const JAPAN_J13_STA_1_IGR:String = "japan-J13_STA_1_IGR";

        public static const JAPAN_J14_TYPE_61:String = "japan-J14_Type_61";

        public static const JAPAN_J15_CHI_NI:String = "japan-J15_Chi_Ni";

        public static const JAPAN_J16_ST_B1:String = "japan-J16_ST_B1";

        public static const JAPAN_J16_ST_B1_IGR:String = "japan-J16_ST_B1_IGR";

        public static const JAPAN_J18_STA_2_3:String = "japan-J18_STA_2_3";

        public static const JAPAN_J19_TIGER_I_JPN:String = "japan-J19_Tiger_I_Jpn";

        public static const JAPAN_J20_TYPE_2605:String = "japan-J20_Type_2605";

        public static const JAPAN_J21_TYPE_91:String = "japan-J21_Type_91";

        public static const JAPAN_J22_TYPE_95:String = "japan-J22_Type_95";

        public static const JAPAN_J22_TYPE_95_BOOTCAMP:String = "japan-J22_Type_95_bootcamp";

        public static const JAPAN_J23_MI_TO:String = "japan-J23_Mi_To";

        public static const JAPAN_J24_MI_TO_130_TONS:String = "japan-J24_Mi_To_130_tons";

        public static const JAPAN_J25_TYPE_4:String = "japan-J25_Type_4";

        public static const JAPAN_J26_TYPE_89:String = "japan-J26_Type_89";

        public static const JAPAN_J27_O_I_120:String = "japan-J27_O_I_120";

        public static const JAPAN_J28_O_I_100:String = "japan-J28_O_I_100";

        public static const JAPAN_J29_NAMELESS:String = "japan-J29_Nameless";

        public static const JAPAN_J30_EDELWEISS:String = "japan-J30_Edelweiss";

        public static const LASTLIT_ALLY_GREEN:String = "lastlit_ally_green";

        public static const LASTLIT_ENEMY_PURPLE:String = "lastlit_enemy_purple";

        public static const LASTLIT_ENEMY_RED:String = "lastlit_enemy_red";

        public static const LASTLIT_SQUADMAN_GOLD:String = "lastlit_squadman_gold";

        public static const LASTLIT_SQUADMAN_YELLOW:String = "lastlit_squadman_yellow";

        public static const LASTLIT_TEAMKILLER_BLUE:String = "lastlit_teamkiller_blue";

        public static const LEFT_STATS_MUTE:String = "left_stats_mute";

        public static const LEVEL1:String = "level1";

        public static const LEVEL10:String = "level10";

        public static const LEVEL2:String = "level2";

        public static const LEVEL3:String = "level3";

        public static const LEVEL4:String = "level4";

        public static const LEVEL5:String = "level5";

        public static const LEVEL6:String = "level6";

        public static const LEVEL7:String = "level7";

        public static const LEVEL8:String = "level8";

        public static const LEVEL9:String = "level9";

        public static const LIGHT_TANK_ALLY_DEAD_GREEN:String = "lightTank_ally_dead_green";

        public static const LIGHT_TANK_ALLY_GREEN:String = "lightTank_ally_green";

        public static const LIGHT_TANK_ENEMY_DEAD_PURPLE:String = "lightTank_enemy_dead_purple";

        public static const LIGHT_TANK_ENEMY_DEAD_RED:String = "lightTank_enemy_dead_red";

        public static const LIGHT_TANK_ENEMY_PURPLE:String = "lightTank_enemy_purple";

        public static const LIGHT_TANK_ENEMY_RED:String = "lightTank_enemy_red";

        public static const LIGHT_TANK_SQUADMAN_DEAD_GOLD:String = "lightTank_squadman_dead_gold";

        public static const LIGHT_TANK_SQUADMAN_DEAD_YELLOW:String = "lightTank_squadman_dead_yellow";

        public static const LIGHT_TANK_SQUADMAN_GOLD:String = "lightTank_squadman_gold";

        public static const LIGHT_TANK_SQUADMAN_YELLOW:String = "lightTank_squadman_yellow";

        public static const LIGHT_TANK_TEAM_KILLER_BLUE:String = "lightTank_teamKiller_blue";

        public static const LIGHT_TANK_TEAM_KILLER_DEAD_BLUE:String = "lightTank_teamKiller_dead_blue";

        public static const LOADER:String = "loader";

        public static const LOADER_ORANGE:String = "loader_orange";

        public static const MEDIUM_TANK_ALLY_DEAD_GREEN:String = "mediumTank_ally_dead_green";

        public static const MEDIUM_TANK_ALLY_GREEN:String = "mediumTank_ally_green";

        public static const MEDIUM_TANK_ENEMY_DEAD_PURPLE:String = "mediumTank_enemy_dead_purple";

        public static const MEDIUM_TANK_ENEMY_DEAD_RED:String = "mediumTank_enemy_dead_red";

        public static const MEDIUM_TANK_ENEMY_PURPLE:String = "mediumTank_enemy_purple";

        public static const MEDIUM_TANK_ENEMY_RED:String = "mediumTank_enemy_red";

        public static const MEDIUM_TANK_SQUADMAN_DEAD_GOLD:String = "mediumTank_squadman_dead_gold";

        public static const MEDIUM_TANK_SQUADMAN_DEAD_YELLOW:String = "mediumTank_squadman_dead_yellow";

        public static const MEDIUM_TANK_SQUADMAN_GOLD:String = "mediumTank_squadman_gold";

        public static const MEDIUM_TANK_SQUADMAN_YELLOW:String = "mediumTank_squadman_yellow";

        public static const MEDIUM_TANK_TEAM_KILLER_BLUE:String = "mediumTank_teamKiller_blue";

        public static const MEDIUM_TANK_TEAM_KILLER_DEAD_BLUE:String = "mediumTank_teamKiller_dead_blue";

        public static const MINIMAP_B1:String = "minimap_b1";

        public static const MINIMAP_B2:String = "minimap_b2";

        public static const MINIMAP_B3:String = "minimap_b3";

        public static const MINIMAP_B4:String = "minimap_b4";

        public static const MINIMAP_B5:String = "minimap_b5";

        public static const MINIMAP_B6:String = "minimap_b6";

        public static const NO_IMAGE:String = "noImage";

        public static const NORMAL:String = "normal";

        public static const NORMAL_BASE:String = "normal_base";

        public static const NORMAL_SIEGE:String = "normal_siege";

        public static const OVERRIDE_BADGE_1:String = "override_badge_1";

        public static const PERSONAL_ARROW_ENTRY:String = "PersonalArrowEntry";

        public static const PERSONAL_GREEN_DIRECTION:String = "PersonalGreenDirection";

        public static const PLAYER_BLACK_MESSAGE_LEFT_RENDERER:String = "PlayerBlackMessageLeftRenderer";

        public static const PLAYER_GREEN_MESSAGE_LEFT_RENDERER:String = "PlayerGreenMessageLeftRenderer";

        public static const PLAYER_RED_MESSAGE_LEFT_RENDERER:String = "PlayerRedMessageLeftRenderer";

        public static const PLAYER_SELF_MESSAGE_LEFT_RENDERER:String = "PlayerSelfMessageLeftRenderer";

        public static const PLAYERS_PANEL_BG:String = "playersPanel_bg";

        public static const PLAYERS_PANEL_DEAD_BG:String = "playersPanel_deadBg";

        public static const PLAYERS_PANEL_SELF_BG:String = "playersPanel_selfBg";

        public static const PLAYERS_PANEL_SWITCH_BG:String = "playersPanelSwitch_bg";

        public static const PLAYERS_PANEL_SWITCH_BT_FULL:String = "playersPanelSwitchBt_full";

        public static const PLAYERS_PANEL_SWITCH_BT_HIDDEN:String = "playersPanelSwitchBt_hidden";

        public static const PLAYERS_PANEL_SWITCH_BT_LONG:String = "playersPanelSwitchBt_long";

        public static const PLAYERS_PANEL_SWITCH_BT_MEDIUM:String = "playersPanelSwitchBt_medium";

        public static const PLAYERS_PANEL_SWITCH_BT_SHORT:String = "playersPanelSwitchBt_short";

        public static const POLAND_PL01_TKS_20MM:String = "poland-Pl01_TKS_20mm";

        public static const POLAND_PL03_PZ_V_POLAND:String = "poland-Pl03_PzV_Poland";

        public static const POLAND_PL05_50_TP_TYSZKIEWICZA:String = "poland-Pl05_50TP_Tyszkiewicza";

        public static const POLAND_PL06_10_TP:String = "poland-Pl06_10TP";

        public static const POLAND_PL07_14_TP:String = "poland-Pl07_14TP";

        public static const POLAND_PL08_50_TP_PROTOTYP:String = "poland-Pl08_50TP_prototyp";

        public static const POLAND_PL09_7_TP:String = "poland-Pl09_7TP";

        public static const POLAND_PL10_40_TP_HABICHA:String = "poland-Pl10_40TP_Habicha";

        public static const POLAND_PL11_45_TP_HABICHA:String = "poland-Pl11_45TP_Habicha";

        public static const POLAND_PL12_25_TP_KSUST_II:String = "poland-Pl12_25TP_KSUST_II";

        public static const POLAND_PL13_53_TP_MARKOWSKIEGO:String = "poland-Pl13_53TP_Markowskiego";

        public static const POLAND_PL14_4_TP:String = "poland-Pl14_4TP";

        public static const POLAND_PL15_60_TP_LEWANDOWSKIEGO:String = "poland-Pl15_60TP_Lewandowskiego";

        public static const POLAND_PL16_T34_85_RUDY:String = "poland-Pl16_T34_85_Rudy";

        public static const POSTMORTEM_PANEL_BG:String = "postmortemPanel_bg";

        public static const POSTMORTEM_PANEL_BG_LARGE:String = "postmortemPanel_bg_large";

        public static const POSTMORTEM_TIPS_BG:String = "postmortemTips_bg";

        public static const PURE_ATSPG_H:String = "pure_atspg_h";

        public static const PURE_ATSPG_M:String = "pure_atspg_m";

        public static const PURE_SPG_H:String = "pure_spg_h";

        public static const PURE_SPG_M:String = "pure_spg_m";

        public static const PURE_TANK_SH:String = "pure_tank_sh";

        public static const PURPLE_TANK_SH_DEAD:String = "purple_tank_sh_dead";

        public static const PURPLE_ABSORPTION_FLAG_ENTRY:String = "PurpleAbsorptionFlagEntry";

        public static const PURPLE_AT_SPG_DESTROYED:String = "purpleAT-SPGDestroyed";

        public static const PURPLE_AT_SPG_NORMAL:String = "purpleAT-SPGNormal";

        public static const PURPLE_FALLOUT_SCORE_PANEL_ARROW:String = "purpleFalloutScorePanelArrow";

        public static const PURPLEHEAVY_TANK_DESTROYED:String = "purpleheavyTankDestroyed";

        public static const PURPLEHEAVY_TANK_NORMAL:String = "purpleheavyTankNormal";

        public static const PURPLELIGHT_TANK_DESTROYED:String = "purplelightTankDestroyed";

        public static const PURPLELIGHT_TANK_NORMAL:String = "purplelightTankNormal";

        public static const PURPLEMEDIUM_TANK_DESTROYED:String = "purplemediumTankDestroyed";

        public static const PURPLEMEDIUM_TANK_NORMAL:String = "purplemediumTankNormal";

        public static const PURPLE_SPG_DESTROYED:String = "purpleSPGDestroyed";

        public static const PURPLE_SPG_NORMAL:String = "purpleSPGNormal";

        public static const PURPLEUNKNOWN_DESTROYED:String = "purpleunknownDestroyed";

        public static const PURPLEUNKNOWN_NORMAL:String = "purpleunknownNormal";

        public static const RADIAL_MENU_BACKGROUND:String = "RadialMenu_background";

        public static const RADIAL_MENU_HOLE:String = "RadialMenu_hole";

        public static const RADIAL_MENU_LIGHT:String = "RadialMenu_light";

        public static const RADIO:String = "radio";

        public static const RADIO_ORANGE:String = "radio_orange";

        public static const RADIO_RED:String = "radio_red";

        public static const RADIOMAN:String = "radioman";

        public static const RADIOMAN_ORANGE:String = "radioman_orange";

        public static const RECON_ENTRY:String = "ReconEntry";

        public static const RED_AT_SPG_DESTROYED:String = "redAT-SPGDestroyed";

        public static const RED_AT_SPG_NORMAL:String = "redAT-SPGNormal";

        public static const RED_FALLOUT_SCORE_PANEL_ARROW:String = "redFalloutScorePanelArrow";

        public static const REDHEAVY_TANK_DESTROYED:String = "redheavyTankDestroyed";

        public static const REDHEAVY_TANK_NORMAL:String = "redheavyTankNormal";

        public static const REDLIGHT_TANK_DESTROYED:String = "redlightTankDestroyed";

        public static const REDLIGHT_TANK_NORMAL:String = "redlightTankNormal";

        public static const REDMEDIUM_TANK_DESTROYED:String = "redmediumTankDestroyed";

        public static const REDMEDIUM_TANK_NORMAL:String = "redmediumTankNormal";

        public static const RED_SPG_DESTROYED:String = "redSPGDestroyed";

        public static const RED_SPG_NORMAL:String = "redSPGNormal";

        public static const REDUNKNOWN_DESTROYED:String = "redunknownDestroyed";

        public static const REDUNKNOWN_NORMAL:String = "redunknownNormal";

        public static const RIBBONS_ARMOR:String = "ribbonsArmor";

        public static const RIBBONS_ASSIST_BY_ABILITY:String = "ribbonsAssistByAbility";

        public static const RIBBONS_ASSIST_SPOT:String = "ribbonsAssistSpot";

        public static const RIBBONS_ASSIST_STUN:String = "ribbonsAssistStun";

        public static const RIBBONS_ASSIST_TRACK:String = "ribbonsAssistTrack";

        public static const RIBBONS_BG_GREEN_LARGE:String = "ribbonsBgGreenLarge";

        public static const RIBBONS_BG_GREEN_MEDIUM:String = "ribbonsBgGreenMedium";

        public static const RIBBONS_BG_GREEN_SMALL:String = "ribbonsBgGreenSmall";

        public static const RIBBONS_BG_GREY_LARGE:String = "ribbonsBgGreyLarge";

        public static const RIBBONS_BG_GREY_MEDIUM:String = "ribbonsBgGreyMedium";

        public static const RIBBONS_BG_GREY_SMALL:String = "ribbonsBgGreySmall";

        public static const RIBBONS_BG_ORANGE_LARGE:String = "ribbonsBgOrangeLarge";

        public static const RIBBONS_BG_ORANGE_MEDIUM:String = "ribbonsBgOrangeMedium";

        public static const RIBBONS_BG_ORANGE_SMALL:String = "ribbonsBgOrangeSmall";

        public static const RIBBONS_BG_PURPLE_LARGE:String = "ribbonsBgPurpleLarge";

        public static const RIBBONS_BG_PURPLE_MEDIUM:String = "ribbonsBgPurpleMedium";

        public static const RIBBONS_BG_PURPLE_SMALL:String = "ribbonsBgPurpleSmall";

        public static const RIBBONS_BG_RED_LARGE:String = "ribbonsBgRedLarge";

        public static const RIBBONS_BG_RED_MEDIUM:String = "ribbonsBgRedMedium";

        public static const RIBBONS_BG_RED_SMALL:String = "ribbonsBgRedSmall";

        public static const RIBBONS_BG_YELLOW_LARGE:String = "ribbonsBgYellowLarge";

        public static const RIBBONS_BG_YELLOW_MEDIUM:String = "ribbonsBgYellowMedium";

        public static const RIBBONS_BG_YELLOW_SMALL:String = "ribbonsBgYellowSmall";

        public static const RIBBONS_BURN:String = "ribbonsBurn";

        public static const RIBBONS_BURN_ENEMY:String = "ribbonsBurnEnemy";

        public static const RIBBONS_BURN_ENEMY_BLIND:String = "ribbonsBurnEnemyBlind";

        public static const RIBBONS_CAPTURE:String = "ribbonsCapture";

        public static const RIBBONS_CRITS:String = "ribbonsCrits";

        public static const RIBBONS_CRITS_ENEMY:String = "ribbonsCritsEnemy";

        public static const RIBBONS_CRITS_ENEMY_BLIND:String = "ribbonsCritsEnemyBlind";

        public static const RIBBONS_DAMAGE:String = "ribbonsDamage";

        public static const RIBBONS_DAMAGE_ENEMY:String = "ribbonsDamageEnemy";

        public static const RIBBONS_DAMAGE_ENEMY_BLIND:String = "ribbonsDamageEnemyBlind";

        public static const RIBBONS_DEFENCE:String = "ribbonsDefence";

        public static const RIBBONS_DEFENDER_BONUS:String = "ribbonsDefenderBonus";

        public static const RIBBONS_DESTRUCTIBLE_DAMAGED:String = "ribbonsDestructibleDamaged";

        public static const RIBBONS_DESTRUCTIBLE_DESTROYED:String = "ribbonsDestructibleDestroyed";

        public static const RIBBONS_DESTRUCTIBLES_DEFENDED:String = "ribbonsDestructiblesDefended";

        public static const RIBBONS_ENEMY_SECTOR_CAPTURED:String = "ribbonsEnemySectorCaptured";

        public static const RIBBONS_EXCLAMATION:String = "ribbonsExclamation";

        public static const RIBBONS_KILL:String = "ribbonsKill";

        public static const RIBBONS_RAM:String = "ribbonsRam";

        public static const RIBBONS_RAM_ENEMY:String = "ribbonsRamEnemy";

        public static const RIBBONS_RAM_ENEMY_BLIND:String = "ribbonsRamEnemyBlind";

        public static const RIBBONS_SPOTTED:String = "ribbonsSpotted";

        public static const RIBBONS_STUN:String = "ribbonsStun";

        public static const RIGHT_STATS_MUTE:String = "right_stats_mute";

        public static const SHOT_SECTOR_LINE:String = "ShotSectorLine";

        public static const SMOKE_ENTRY:String = "SmokeEntry";

        public static const SPG_ALLY_DEAD_GREEN:String = "SPG_ally_dead_green";

        public static const SPG_ALLY_GREEN:String = "SPG_ally_green";

        public static const SPG_ENEMY_DEAD_PURPLE:String = "SPG_enemy_dead_purple";

        public static const SPG_ENEMY_DEAD_RED:String = "SPG_enemy_dead_red";

        public static const SPG_ENEMY_PURPLE:String = "SPG_enemy_purple";

        public static const SPG_ENEMY_RED:String = "SPG_enemy_red";

        public static const SPG_SQUADMAN_DEAD_GOLD:String = "SPG_squadman_dead_gold";

        public static const SPG_SQUADMAN_DEAD_YELLOW:String = "SPG_squadman_dead_yellow";

        public static const SPG_SQUADMAN_GOLD:String = "SPG_squadman_gold";

        public static const SPG_SQUADMAN_YELLOW:String = "SPG_squadman_yellow";

        public static const SPG_TEAM_KILLER_BLUE:String = "SPG_teamKiller_blue";

        public static const SPG_TEAM_KILLER_DEAD_BLUE:String = "SPG_teamKiller_dead_blue";

        public static const SQUAD_GOLD_1:String = "squad_gold_1";

        public static const SQUAD_GOLD_10:String = "squad_gold_10";

        public static const SQUAD_GOLD_11:String = "squad_gold_11";

        public static const SQUAD_GOLD_12:String = "squad_gold_12";

        public static const SQUAD_GOLD_13:String = "squad_gold_13";

        public static const SQUAD_GOLD_14:String = "squad_gold_14";

        public static const SQUAD_GOLD_2:String = "squad_gold_2";

        public static const SQUAD_GOLD_3:String = "squad_gold_3";

        public static const SQUAD_GOLD_4:String = "squad_gold_4";

        public static const SQUAD_GOLD_5:String = "squad_gold_5";

        public static const SQUAD_GOLD_6:String = "squad_gold_6";

        public static const SQUAD_GOLD_7:String = "squad_gold_7";

        public static const SQUAD_GOLD_8:String = "squad_gold_8";

        public static const SQUAD_GOLD_9:String = "squad_gold_9";

        public static const SQUAD_INVITE_DISABLED:String = "squad_inviteDisabled";

        public static const SQUAD_INVITE_RECEIVED:String = "squad_inviteReceived";

        public static const SQUAD_INVITE_RECEIVED_FROM_SQUAD:String = "squad_inviteReceivedFromSquad";

        public static const SQUAD_INVITE_SENT:String = "squad_inviteSent";

        public static const SQUAD_NO_SOUND:String = "squad_noSound";

        public static const SQUAD_SILVER_1:String = "squad_silver_1";

        public static const SQUAD_SILVER_10:String = "squad_silver_10";

        public static const SQUAD_SILVER_11:String = "squad_silver_11";

        public static const SQUAD_SILVER_12:String = "squad_silver_12";

        public static const SQUAD_SILVER_13:String = "squad_silver_13";

        public static const SQUAD_SILVER_14:String = "squad_silver_14";

        public static const SQUAD_SILVER_2:String = "squad_silver_2";

        public static const SQUAD_SILVER_3:String = "squad_silver_3";

        public static const SQUAD_SILVER_4:String = "squad_silver_4";

        public static const SQUAD_SILVER_5:String = "squad_silver_5";

        public static const SQUAD_SILVER_6:String = "squad_silver_6";

        public static const SQUAD_SILVER_7:String = "squad_silver_7";

        public static const SQUAD_SILVER_8:String = "squad_silver_8";

        public static const SQUAD_SILVER_9:String = "squad_silver_9";

        public static const STATS_BG:String = "stats_bg";

        public static const STATS_MUTE:String = "stats_mute";

        public static const STATS_TAB_BG:String = "stats_tab_bg";

        public static const STATS_TABLE_BG:String = "stats_table_bg";

        public static const STATS_TABLE_FRAGS:String = "stats_table_frags";

        public static const STATS_TABLE_PLATOON:String = "stats_table_platoon";

        public static const STATS_TABLE_TANK:String = "stats_table_tank";

        public static const STRIP_56:String = "strip_56";

        public static const STRIP_57:String = "strip_57";

        public static const SURVEYING_DEVICE:String = "surveyingDevice";

        public static const SURVEYING_DEVICE_ORANGE:String = "surveyingDevice_orange";

        public static const SURVEYING_DEVICE_RED:String = "surveyingDevice_red";

        public static const SWEDEN_S01_STRV_74_A2:String = "sweden-S01_Strv_74_A2";

        public static const SWEDEN_S02_STRV_M42:String = "sweden-S02_Strv_M42";

        public static const SWEDEN_S03_STRV_M38:String = "sweden-S03_Strv_M38";

        public static const SWEDEN_S04_LAGO_I:String = "sweden-S04_Lago_I";

        public static const SWEDEN_S05_STRV_M21_29:String = "sweden-S05_Strv_M21_29";

        public static const SWEDEN_S06_IKV_90_TYP_B_BOFORS:String = "sweden-S06_Ikv_90_Typ_B_Bofors";

        public static const SWEDEN_S07_STRV_74:String = "sweden-S07_Strv_74";

        public static const SWEDEN_S08_IKV_65_ALT_2:String = "sweden-S08_Ikv_65_Alt_2";

        public static const SWEDEN_S09_L_120_TD:String = "sweden-S09_L_120_TD";

        public static const SWEDEN_S10_STRV_103_0_SERIES:String = "sweden-S10_Strv_103_0_Series";

        public static const SWEDEN_S11_STRV_103_B:String = "sweden-S11_Strv_103B";

        public static const SWEDEN_S12_STRV_M40:String = "sweden-S12_Strv_M40";

        public static const SWEDEN_S13_LEO:String = "sweden-S13_Leo";

        public static const SWEDEN_S14_IKV_103:String = "sweden-S14_Ikv_103";

        public static const SWEDEN_S15_L_60:String = "sweden-S15_L_60";

        public static const SWEDEN_S16_KRANVAGN:String = "sweden-S16_Kranvagn";

        public static const SWEDEN_S17_EMIL_1952_E2:String = "sweden-S17_EMIL_1952_E2";

        public static const SWEDEN_S18_EMIL_1951_E1:String = "sweden-S18_EMIL_1951_E1";

        public static const SWEDEN_S19_SAV_M43:String = "sweden-S19_Sav_M43";

        public static const SWEDEN_S20_IKV_72:String = "sweden-S20_Ikv_72";

        public static const SWEDEN_S21_UDES_03:String = "sweden-S21_UDES_03";

        public static const SWEDEN_S22_STRV_S1:String = "sweden-S22_Strv_S1";

        public static const SWEDEN_S22_STRV_S1_FL:String = "sweden-S22_Strv_S1_FL";

        public static const SWEDEN_S23_STRV_81:String = "sweden-S23_Strv_81";

        public static const SWEDEN_S23_STRV_81_SABATON:String = "sweden-S23_Strv_81_sabaton";

        public static const SWEDEN_S25_EMIL_51:String = "sweden-S25_EMIL_51";

        public static const SWEDEN_S26_LANSEN_C:String = "sweden-S26_Lansen_C";

        public static const SWEDEN_S27_UDES_16:String = "sweden-S27_UDES_16";

        public static const SWEDEN_S28_UDES_15_16:String = "sweden-S28_UDES_15_16";

        public static const SWEDEN_S28_UDES_15_16_BOB:String = "sweden-S28_UDES_15_16_bob";

        public static const SWEDEN_S29_UDES_14_5:String = "sweden-S29_UDES_14_5";

        public static const SWEDEN_S30_UDES_03_ALT_3:String = "sweden-S30_UDES_03_Alt_3";

        public static const SWEDEN_S31_STRV_K:String = "sweden-S31_Strv_K";

        public static const SWITCH_SIEGE_DAMAGED:String = "switch_siege_damaged";

        public static const TOXIC_BG:String = "toxic_bg";

        public static const TURRET_ROTATOR:String = "turretRotator";

        public static const TURRET_ROTATOR_ORANGE:String = "turretRotator_orange";

        public static const TURRET_ROTATOR_RED:String = "turretRotator_red";

        public static const TUTORIAL_ENTRY:String = "TutorialEntry";

        public static const UK_GB01_MEDIUM_MARK_I:String = "uk-GB01_Medium_Mark_I";

        public static const UK_GB01_MEDIUM_MARK_I_BOT:String = "uk-GB01_Medium_Mark_I_bot";

        public static const UK_GB03_CRUISER_MK_I:String = "uk-GB03_Cruiser_Mk_I";

        public static const UK_GB04_VALENTINE:String = "uk-GB04_Valentine";

        public static const UK_GB05_VICKERS_MEDIUM_MK_II:String = "uk-GB05_Vickers_Medium_Mk_II";

        public static const UK_GB05_VICKERS_MEDIUM_MK_II_BOT:String = "uk-GB05_Vickers_Medium_Mk_II_bot";

        public static const UK_GB06_VICKERS_MEDIUM_MK_III:String = "uk-GB06_Vickers_Medium_Mk_III";

        public static const UK_GB07_MATILDA:String = "uk-GB07_Matilda";

        public static const UK_GB08_CHURCHILL_I:String = "uk-GB08_Churchill_I";

        public static const UK_GB08_CHURCHILL_I_IGR:String = "uk-GB08_Churchill_I_IGR";

        public static const UK_GB09_CHURCHILL_VII:String = "uk-GB09_Churchill_VII";

        public static const UK_GB10_BLACK_PRINCE:String = "uk-GB10_Black_Prince";

        public static const UK_GB100_MANTICORE:String = "uk-GB100_Manticore";

        public static const UK_GB101_FV1066_SENLAC:String = "uk-GB101_FV1066_Senlac";

        public static const UK_GB102_LHMTV:String = "uk-GB102_LHMTV";

        public static const UK_GB103_GSOR3301_AVR_FS:String = "uk-GB103_GSOR3301_AVR_FS";

        public static const UK_GB104_GSR_3301_SETTER:String = "uk-GB104_GSR_3301_Setter";

        public static const UK_GB105_BLACK_PRINCE_2019:String = "uk-GB105_Black_Prince_2019";

        public static const UK_GB107_CAVALIER:String = "uk-GB107_Cavalier";

        public static const UK_GB108_A46:String = "uk-GB108_A46";

        public static const UK_GB11_CAERNARVON:String = "uk-GB11_Caernarvon";

        public static const UK_GB11_CAERNARVON_IGR:String = "uk-GB11_Caernarvon_IGR";

        public static const UK_GB12_CONQUEROR:String = "uk-GB12_Conqueror";

        public static const UK_GB13_FV215B:String = "uk-GB13_FV215b";

        public static const UK_GB13_FV215B_IGR:String = "uk-GB13_FV215b_IGR";

        public static const UK_GB14_M2:String = "uk-GB14_M2";

        public static const UK_GB15_STUART_I:String = "uk-GB15_Stuart_I";

        public static const UK_GB17_GRANT_I:String = "uk-GB17_Grant_I";

        public static const UK_GB19_SHERMAN_FIREFLY:String = "uk-GB19_Sherman_Firefly";

        public static const UK_GB20_CRUSADER:String = "uk-GB20_Crusader";

        public static const UK_GB21_CROMWELL:String = "uk-GB21_Cromwell";

        public static const UK_GB21_CROMWELL_IGR:String = "uk-GB21_Cromwell_IGR";

        public static const UK_GB22_COMET:String = "uk-GB22_Comet";

        public static const UK_GB23_CENTURION:String = "uk-GB23_Centurion";

        public static const UK_GB23_CENTURION_IGR:String = "uk-GB23_Centurion_IGR";

        public static const UK_GB24_CENTURION_MK3:String = "uk-GB24_Centurion_Mk3";

        public static const UK_GB25_LOYD_GUN_CARRIAGE:String = "uk-GB25_Loyd_Gun_Carriage";

        public static const UK_GB26_BIRCH_GUN:String = "uk-GB26_Birch_Gun";

        public static const UK_GB27_SEXTON:String = "uk-GB27_Sexton";

        public static const UK_GB28_BISHOP:String = "uk-GB28_Bishop";

        public static const UK_GB29_CRUSADER_5INCH:String = "uk-GB29_Crusader_5inch";

        public static const UK_GB30_FV3805:String = "uk-GB30_FV3805";

        public static const UK_GB31_CONQUEROR_GUN:String = "uk-GB31_Conqueror_Gun";

        public static const UK_GB32_TORTOISE:String = "uk-GB32_Tortoise";

        public static const UK_GB33_SENTINEL_AC_I:String = "uk-GB33_Sentinel_AC_I";

        public static const UK_GB35_SENTINEL_AC_IV:String = "uk-GB35_Sentinel_AC_IV";

        public static const UK_GB37_VALLIANT:String = "uk-GB37_Valliant";

        public static const UK_GB39_UNIVERSAL_CARRIER_QF2:String = "uk-GB39_Universal_CarrierQF2";

        public static const UK_GB40_GUN_CARRIER_CHURCHILL:String = "uk-GB40_Gun_Carrier_Churchill";

        public static const UK_GB41_CHALLENGER:String = "uk-GB41_Challenger";

        public static const UK_GB42_VALENTINE_AT:String = "uk-GB42_Valentine_AT";

        public static const UK_GB44_ARCHER:String = "uk-GB44_Archer";

        public static const UK_GB45_ACHILLES_IIC:String = "uk-GB45_Achilles_IIC";

        public static const UK_GB48_FV215B_183:String = "uk-GB48_FV215b_183";

        public static const UK_GB50_SHERMAN_III:String = "uk-GB50_Sherman_III";

        public static const UK_GB51_EXCELSIOR:String = "uk-GB51_Excelsior";

        public static const UK_GB52_A45:String = "uk-GB52_A45";

        public static const UK_GB57_ALECTO:String = "uk-GB57_Alecto";

        public static const UK_GB58_CRUISER_MK_III:String = "uk-GB58_Cruiser_Mk_III";

        public static const UK_GB59_CRUISER_MK_IV:String = "uk-GB59_Cruiser_Mk_IV";

        public static const UK_GB60_COVENANTER:String = "uk-GB60_Covenanter";

        public static const UK_GB63_TOG_II:String = "uk-GB63_TOG_II";

        public static const UK_GB68_MATILDA_BLACK_PRINCE:String = "uk-GB68_Matilda_Black_Prince";

        public static const UK_GB69_CRUISER_MK_II:String = "uk-GB69_Cruiser_Mk_II";

        public static const UK_GB70_FV4202_105:String = "uk-GB70_FV4202_105";

        public static const UK_GB70_N_FV4202_105:String = "uk-GB70_N_FV4202_105";

        public static const UK_GB70_N_FV4202_105_FL:String = "uk-GB70_N_FV4202_105_FL";

        public static const UK_GB71_AT_15_A:String = "uk-GB71_AT_15A";

        public static const UK_GB72_AT15:String = "uk-GB72_AT15";

        public static const UK_GB72_AT15_IGR:String = "uk-GB72_AT15_IGR";

        public static const UK_GB73_AT2:String = "uk-GB73_AT2";

        public static const UK_GB74_AT8:String = "uk-GB74_AT8";

        public static const UK_GB75_AT7:String = "uk-GB75_AT7";

        public static const UK_GB76_MK_VIC:String = "uk-GB76_Mk_VIC";

        public static const UK_GB77_FV304:String = "uk-GB77_FV304";

        public static const UK_GB77_FV304_IGR:String = "uk-GB77_FV304_IGR";

        public static const UK_GB78_SEXTON_I:String = "uk-GB78_Sexton_I";

        public static const UK_GB79_FV206:String = "uk-GB79_FV206";

        public static const UK_GB80_CHARIOTEER:String = "uk-GB80_Charioteer";

        public static const UK_GB81_FV4004:String = "uk-GB81_FV4004";

        public static const UK_GB83_FV4005:String = "uk-GB83_FV4005";

        public static const UK_GB84_CHIEFTAIN_MK6:String = "uk-GB84_Chieftain_Mk6";

        public static const UK_GB85_CROMWELL_BERLIN:String = "uk-GB85_Cromwell_Berlin";

        public static const UK_GB86_CENTURION_ACTION_X:String = "uk-GB86_Centurion_Action_X";

        public static const UK_GB87_CHIEFTAIN_T95_TURRET:String = "uk-GB87_Chieftain_T95_turret";

        public static const UK_GB88_T95_CHIEFTAIN_TURRET:String = "uk-GB88_T95_Chieftain_turret";

        public static const UK_GB91_SUPER_CONQUEROR:String = "uk-GB91_Super_Conqueror";

        public static const UK_GB92_FV217:String = "uk-GB92_FV217";

        public static const UK_GB93_CAERNARVON_AX:String = "uk-GB93_Caernarvon_AX";

        public static const UK_GB94_CENTURION_MK5_1_RAAC:String = "uk-GB94_Centurion_Mk5-1_RAAC";

        public static const UK_GB95_EKINS_FIREFLY_M4_A4:String = "uk-GB95_Ekins_Firefly_M4A4";

        public static const UK_GB96_EXCALIBUR:String = "uk-GB96_Excalibur";

        public static const UK_GB97_CHIMERA:String = "uk-GB97_Chimera";

        public static const UK_GB98_T95_FV4201_CHIEFTAIN:String = "uk-GB98_T95_FV4201_Chieftain";

        public static const UK_GB99_TURTLE_MK1:String = "uk-GB99_Turtle_Mk1";

        public static const UNKNOWN:String = "unknown";

        public static const UNKNOWN_DEAD_PURPLE:String = "unknown_dead_purple";

        public static const UNKNOWN_DEAD_RED:String = "unknown_dead_red";

        public static const USA_A01_T1_CUNNINGHAM:String = "usa-A01_T1_Cunningham";

        public static const USA_A01_T1_CUNNINGHAM_BOT:String = "usa-A01_T1_Cunningham_bot";

        public static const USA_A02_M2_LT:String = "usa-A02_M2_lt";

        public static const USA_A03_M3_STUART:String = "usa-A03_M3_Stuart";

        public static const USA_A03_M3_STUART_BOOTCAMP:String = "usa-A03_M3_Stuart_bootcamp";

        public static const USA_A04_M3_GRANT:String = "usa-A04_M3_Grant";

        public static const USA_A05_M4_SHERMAN:String = "usa-A05_M4_Sherman";

        public static const USA_A05_M4_SHERMAN_IGR:String = "usa-A05_M4_Sherman_IGR";

        public static const USA_A06_M4_A3_E8_SHERMAN:String = "usa-A06_M4A3E8_Sherman";

        public static const USA_A06_M4_A3_E8_SHERMAN_TRAINING:String = "usa-A06_M4A3E8_Sherman_training";

        public static const USA_A07_T20:String = "usa-A07_T20";

        public static const USA_A09_T1_HVY:String = "usa-A09_T1_hvy";

        public static const USA_A10_M6:String = "usa-A10_M6";

        public static const USA_A100_T49:String = "usa-A100_T49";

        public static const USA_A101_M56:String = "usa-A101_M56";

        public static const USA_A102_T28_CONCEPT:String = "usa-A102_T28_concept";

        public static const USA_A103_T71_E1:String = "usa-A103_T71E1";

        public static const USA_A103_T71_E1_IGR:String = "usa-A103_T71E1_IGR";

        public static const USA_A104_M4_A3_E8_A:String = "usa-A104_M4A3E8A";

        public static const USA_A106_M48_A2_120:String = "usa-A106_M48A2_120";

        public static const USA_A107_T1_HMC:String = "usa-A107_T1_HMC";

        public static const USA_A108_T18_HMC:String = "usa-A108_T18_HMC";

        public static const USA_A109_T56_GMC:String = "usa-A109_T56_GMC";

        public static const USA_A11_T29:String = "usa-A11_T29";

        public static const USA_A11_T29_IGR:String = "usa-A11_T29_IGR";

        public static const USA_A111_T25_PILOT:String = "usa-A111_T25_Pilot";

        public static const USA_A112_T71_E2:String = "usa-A112_T71E2";

        public static const USA_A112_T71_E2_R:String = "usa-A112_T71E2R";

        public static const USA_A115_CHRYSLER_K:String = "usa-A115_Chrysler_K";

        public static const USA_A115_CHRYSLER_K_GF:String = "usa-A115_Chrysler_K_GF";

        public static const USA_A116_XM551:String = "usa-A116_XM551";

        public static const USA_A117_T26_E5:String = "usa-A117_T26E5";

        public static const USA_A117_T26_E5_FL:String = "usa-A117_T26E5_FL";

        public static const USA_A117_T26_E5_PATRIOT:String = "usa-A117_T26E5_Patriot";

        public static const USA_A118_M4_THUNDERBOLT:String = "usa-A118_M4_Thunderbolt";

        public static const USA_A12_T32:String = "usa-A12_T32";

        public static const USA_A12_T32_FL:String = "usa-A12_T32_FL";

        public static const USA_A120_M48_A5:String = "usa-A120_M48A5";

        public static const USA_A120_M48_A5_IGR:String = "usa-A120_M48A5_IGR";

        public static const USA_A121_M26_COLOGNE:String = "usa-A121_M26_Cologne";

        public static const USA_A122_TS_5:String = "usa-A122_TS-5";

        public static const USA_A123_T78:String = "usa-A123_T78";

        public static const USA_A124_T54_E2:String = "usa-A124_T54E2";

        public static const USA_A125_AEP_1:String = "usa-A125_AEP_1";

        public static const USA_A126_PZ_VI_TIGER_II_CAPT:String = "usa-A126_PzVI_Tiger_II_capt";

        public static const USA_A127_TL_1_LPC:String = "usa-A127_TL_1_LPC";

        public static const USA_A128_CONCEPT_1B:String = "usa-A128_concept_1b";

        public static const USA_A129_T6_MEDIUM:String = "usa-A129_T6_medium";

        public static const USA_A13_T34_HVY:String = "usa-A13_T34_hvy";

        public static const USA_A13_T34_HVY_BF:String = "usa-A13_T34_hvy_BF";

        public static const USA_A13_T34_HVY_FL:String = "usa-A13_T34_hvy_FL";

        public static const USA_A13_T34_HVY_IGR:String = "usa-A13_T34_hvy_IGR";

        public static const USA_A130_SUPER_HELLCAT:String = "usa-A130_Super_Hellcat";

        public static const USA_A14_T30:String = "usa-A14_T30";

        public static const USA_A15_T57:String = "usa-A15_T57";

        public static const USA_A16_M7_PRIEST:String = "usa-A16_M7_Priest";

        public static const USA_A17_M37:String = "usa-A17_M37";

        public static const USA_A18_M41:String = "usa-A18_M41";

        public static const USA_A19_T2_LT:String = "usa-A19_T2_lt";

        public static const USA_A21_T14:String = "usa-A21_T14";

        public static const USA_A22_M5_STUART:String = "usa-A22_M5_Stuart";

        public static const USA_A22_M5_STUART_BOOTCAMP:String = "usa-A22_M5_Stuart_bootcamp";

        public static const USA_A23_M7_MED:String = "usa-A23_M7_med";

        public static const USA_A24_T2_MED:String = "usa-A24_T2_med";

        public static const USA_A24_T2_MED_BOT:String = "usa-A24_T2_med_bot";

        public static const USA_A25_M2_MED:String = "usa-A25_M2_med";

        public static const USA_A25_M2_MED_BOOTCAMP:String = "usa-A25_M2_med_bootcamp";

        public static const USA_A26_T18:String = "usa-A26_T18";

        public static const USA_A27_T82:String = "usa-A27_T82";

        public static const USA_A29_T40:String = "usa-A29_T40";

        public static const USA_A30_M10_WOLVERINE:String = "usa-A30_M10_Wolverine";

        public static const USA_A31_M36_SLAGGER:String = "usa-A31_M36_Slagger";

        public static const USA_A32_M12:String = "usa-A32_M12";

        public static const USA_A33_MTLS_1_G14:String = "usa-A33_MTLS-1G14";

        public static const USA_A34_M24_CHAFFEE:String = "usa-A34_M24_Chaffee";

        public static const USA_A35_PERSHING:String = "usa-A35_Pershing";

        public static const USA_A35_PERSHING_IGR:String = "usa-A35_Pershing_IGR";

        public static const USA_A36_SHERMAN_JUMBO:String = "usa-A36_Sherman_Jumbo";

        public static const USA_A36_SHERMAN_JUMBO_BOOTCAMP:String = "usa-A36_Sherman_Jumbo_bootcamp";

        public static const USA_A36_SHERMAN_JUMBO_IGR:String = "usa-A36_Sherman_Jumbo_IGR";

        public static const USA_A37_M40_M43:String = "usa-A37_M40M43";

        public static const USA_A38_T92:String = "usa-A38_T92";

        public static const USA_A39_T28:String = "usa-A39_T28";

        public static const USA_A40_T95:String = "usa-A40_T95";

        public static const USA_A41_M18_HELLCAT:String = "usa-A41_M18_Hellcat";

        public static const USA_A43_M22_LOCUST:String = "usa-A43_M22_Locust";

        public static const USA_A44_M4_A2_E4:String = "usa-A44_M4A2E4";

        public static const USA_A45_M6_A2_E1:String = "usa-A45_M6A2E1";

        public static const USA_A46_T3:String = "usa-A46_T3";

        public static const USA_A57_M8_A1:String = "usa-A57_M8A1";

        public static const USA_A58_T67:String = "usa-A58_T67";

        public static const USA_A58_T67_IGR:String = "usa-A58_T67_IGR";

        public static const USA_A62_RAM_II:String = "usa-A62_Ram-II";

        public static const USA_A63_M46_PATTON:String = "usa-A63_M46_Patton";

        public static const USA_A63_M46_PATTON_KR:String = "usa-A63_M46_Patton_KR";

        public static const USA_A64_T25_AT:String = "usa-A64_T25_AT";

        public static const USA_A66_M103:String = "usa-A66_M103";

        public static const USA_A67_T57_58:String = "usa-A67_T57_58";

        public static const USA_A68_T28_PROTOTYPE:String = "usa-A68_T28_Prototype";

        public static const USA_A69_T110_E5:String = "usa-A69_T110E5";

        public static const USA_A71_T21:String = "usa-A71_T21";

        public static const USA_A72_T25_2:String = "usa-A72_T25_2";

        public static const USA_A74_T1_E6:String = "usa-A74_T1_E6";

        public static const USA_A78_M4_IMPROVED:String = "usa-A78_M4_Improved";

        public static const USA_A80_T26_E4_SUPER_PERSHING:String = "usa-A80_T26_E4_SuperPershing";

        public static const USA_A80_T26_E4_SUPER_PERSHING_FL:String = "usa-A80_T26_E4_SuperPershing_FL";

        public static const USA_A81_T95_E2:String = "usa-A81_T95_E2";

        public static const USA_A83_T110_E4:String = "usa-A83_T110E4";

        public static const USA_A85_T110_E3:String = "usa-A85_T110E3";

        public static const USA_A86_T23_E3:String = "usa-A86_T23E3";

        public static const USA_A87_M44:String = "usa-A87_M44";

        public static const USA_A88_M53_55:String = "usa-A88_M53_55";

        public static const USA_A89_T54_E1:String = "usa-A89_T54E1";

        public static const USA_A90_T69:String = "usa-A90_T69";

        public static const USA_A90_T69_IGR:String = "usa-A90_T69_IGR";

        public static const USA_A92_M60:String = "usa-A92_M60";

        public static const USA_A93_T7_COMBAT_CAR:String = "usa-A93_T7_Combat_Car";

        public static const USA_A94_T37:String = "usa-A94_T37";

        public static const USA_A95_T95_E6:String = "usa-A95_T95_E6";

        public static const USA_A97_M41_BULLDOG:String = "usa-A97_M41_Bulldog";

        public static const USA_A99_T92_LT:String = "usa-A99_T92_LT";

        public static const USSR_OBSERVER:String = "ussr-Observer";

        public static const USSR_R01_IS:String = "ussr-R01_IS";

        public static const USSR_R02_SU_85:String = "ussr-R02_SU-85";

        public static const USSR_R03_BT_7:String = "ussr-R03_BT-7";

        public static const USSR_R03_BT_7_BOOTCAMP:String = "ussr-R03_BT-7_bootcamp";

        public static const USSR_R04_T_34:String = "ussr-R04_T-34";

        public static const USSR_R05_KV:String = "ussr-R05_KV";

        public static const USSR_R06_T_28:String = "ussr-R06_T-28";

        public static const USSR_R06_T_28_BOOTCAMP:String = "ussr-R06_T-28_bootcamp";

        public static const USSR_R07_T_34_85:String = "ussr-R07_T-34-85";

        public static const USSR_R07_T_34_85_BOOTCAMP:String = "ussr-R07_T-34-85_bootcamp";

        public static const USSR_R07_T_34_85_TRAINING:String = "ussr-R07_T-34-85_training";

        public static const USSR_R08_BT_2:String = "ussr-R08_BT-2";

        public static const USSR_R08_BT_2_BOOTCAMP:String = "ussr-R08_BT-2_bootcamp";

        public static const USSR_R09_T_26:String = "ussr-R09_T-26";

        public static const USSR_R09_T_26_BOT:String = "ussr-R09_T-26_bot";

        public static const USSR_R10_AT_1:String = "ussr-R10_AT-1";

        public static const USSR_R100_SU122_A:String = "ussr-R100_SU122A";

        public static const USSR_R101_MT25:String = "ussr-R101_MT25";

        public static const USSR_R104_OBJECT_430_II:String = "ussr-R104_Object_430_II";

        public static const USSR_R105_BT_7_A:String = "ussr-R105_BT_7A";

        public static const USSR_R106_KV85:String = "ussr-R106_KV85";

        public static const USSR_R106_KV85_IGR:String = "ussr-R106_KV85_IGR";

        public static const USSR_R107_LTB:String = "ussr-R107_LTB";

        public static const USSR_R108_T34_85_M:String = "ussr-R108_T34_85M";

        public static const USSR_R109_T54_S:String = "ussr-R109_T54S";

        public static const USSR_R11_MS_1:String = "ussr-R11_MS-1";

        public static const USSR_R11_MS_1_BOT:String = "ussr-R11_MS-1_bot";

        public static const USSR_R110_OBJECT_260:String = "ussr-R110_Object_260";

        public static const USSR_R111_ISU130:String = "ussr-R111_ISU130";

        public static const USSR_R112_T54_45:String = "ussr-R112_T54_45";

        public static const USSR_R112_T54_45_FL:String = "ussr-R112_T54_45_FL";

        public static const USSR_R113_OBJECT_730:String = "ussr-R113_Object_730";

        public static const USSR_R114_OBJECT_244:String = "ussr-R114_Object_244";

        public static const USSR_R115_IS_3_AUTO:String = "ussr-R115_IS-3_auto";

        public static const USSR_R115_IS_3_AUTO_TEST:String = "ussr-R115_IS-3_auto_test";

        public static const USSR_R116_ISU122_C_BERLIN:String = "ussr-R116_ISU122C_Berlin";

        public static const USSR_R117_T34_85_RUDY:String = "ussr-R117_T34_85_Rudy";

        public static const USSR_R118_T28_F30:String = "ussr-R118_T28_F30";

        public static const USSR_R119_OBJECT_777:String = "ussr-R119_Object_777";

        public static const USSR_R119_OBJECT_777_C:String = "ussr-R119_Object_777C";

        public static const USSR_R12_A_20:String = "ussr-R12_A-20";

        public static const USSR_R120_T22_SR_A22:String = "ussr-R120_T22SR_A22";

        public static const USSR_R121_KV4_KTT:String = "ussr-R121_KV4_KTT";

        public static const USSR_R122_T44_100:String = "ussr-R122_T44_100";

        public static const USSR_R122_T44_100_B:String = "ussr-R122_T44_100B";

        public static const USSR_R123_KIROVETS_1:String = "ussr-R123_Kirovets_1";

        public static const USSR_R125_T_45:String = "ussr-R125_T_45";

        public static const USSR_R126_OBJECT_730_5:String = "ussr-R126_Object_730_5";

        public static const USSR_R127_T44_100_K:String = "ussr-R127_T44_100_K";

        public static const USSR_R127_T44_100_P:String = "ussr-R127_T44_100_P";

        public static const USSR_R127_T44_100_U:String = "ussr-R127_T44_100_U";

        public static const USSR_R128_KV4_KRESLAVSKIY:String = "ussr-R128_KV4_Kreslavskiy";

        public static const USSR_R129_OBJECT_257:String = "ussr-R129_Object_257";

        public static const USSR_R13_KV_1S:String = "ussr-R13_KV-1s";

        public static const USSR_R131_TANK_GAVALOV:String = "ussr-R131_Tank_Gavalov";

        public static const USSR_R132_VNII_100_LT:String = "ussr-R132_VNII_100LT";

        public static const USSR_R133_KV_122:String = "ussr-R133_KV_122";

        public static const USSR_R134_OBJECT_252_K:String = "ussr-R134_Object_252K";

        public static const USSR_R134_OBJECT_252_U:String = "ussr-R134_Object_252U";

        public static const USSR_R135_T_103:String = "ussr-R135_T_103";

        public static const USSR_R139_IS_M:String = "ussr-R139_IS_M";

        public static const USSR_R14_SU_5:String = "ussr-R14_SU-5";

        public static const USSR_R140_M4_LOZA:String = "ussr-R140_M4_Loza";

        public static const USSR_R143_T_29:String = "ussr-R143_T_29";

        public static const USSR_R144_K_91:String = "ussr-R144_K_91";

        public static const USSR_R145_OBJECT_705_A:String = "ussr-R145_Object_705_A";

        public static const USSR_R146_STG:String = "ussr-R146_STG";

        public static const USSR_R146_STG_TDAY:String = "ussr-R146_STG_Tday";

        public static const USSR_R148_OBJECT_430_U:String = "ussr-R148_Object_430_U";

        public static const USSR_R149_OBJECT_268_4:String = "ussr-R149_Object_268_4";

        public static const USSR_R15_S_51:String = "ussr-R15_S-51";

        public static const USSR_R151_OBJECT_257_2:String = "ussr-R151_Object_257_2";

        public static const USSR_R152_KV2_W:String = "ussr-R152_KV2_W";

        public static const USSR_R153_OBJECT_705:String = "ussr-R153_Object_705";

        public static const USSR_R154_T_34_E_1943:String = "ussr-R154_T_34E_1943";

        public static const USSR_R155_OBJECT_277:String = "ussr-R155_Object_277";

        public static const USSR_R156_IS_2_M:String = "ussr-R156_IS_2M";

        public static const USSR_R157_OBJECT_279_R:String = "ussr-R157_Object_279R";

        public static const USSR_R158_LT_432:String = "ussr-R158_LT_432";

        public static const USSR_R159_SU_130_PM:String = "ussr-R159_SU_130PM";

        public static const USSR_R16_SU_18:String = "ussr-R16_SU-18";

        public static const USSR_R160_T_50_2:String = "ussr-R160_T_50_2";

        public static const USSR_R161_T_116:String = "ussr-R161_T_116";

        public static const USSR_R165_OBJECT_703_II:String = "ussr-R165_Object_703_II";

        public static const USSR_R165_OBJECT_703_II_2:String = "ussr-R165_Object_703_II_2";

        public static const USSR_R169_ST_II:String = "ussr-R169_ST_II";

        public static const USSR_R17_SU_100:String = "ussr-R17_SU-100";

        public static const USSR_R170_IS_2_II:String = "ussr-R170_IS_2_II";

        public static const USSR_R171_IS_3_II:String = "ussr-R171_IS_3_II";

        public static const USSR_R172_OBJECT_752:String = "ussr-R172_Object_752";

        public static const USSR_R173_K_91_2:String = "ussr-R173_K_91_2";

        public static const USSR_R174_BT_5:String = "ussr-R174_BT-5";

        public static const USSR_R175_IS_2_SCREEN:String = "ussr-R175_IS_2_screen";

        public static const USSR_R177_ISU_152_K_BL10:String = "ussr-R177_ISU_152K_BL10";

        public static const USSR_R178_OBJECT_780:String = "ussr-R178_Object_780";

        public static const USSR_R18_SU_152:String = "ussr-R18_SU-152";

        public static const USSR_R18_SU_152_IGR:String = "ussr-R18_SU-152_IGR";

        public static const USSR_R19_IS_3:String = "ussr-R19_IS-3";

        public static const USSR_R19_IS_3_IGR:String = "ussr-R19_IS-3_IGR";

        public static const USSR_R20_T_44:String = "ussr-R20_T-44";

        public static const USSR_R20_T_44_FL:String = "ussr-R20_T-44_FL";

        public static const USSR_R20_T_44_IGR:String = "ussr-R20_T-44_IGR";

        public static const USSR_R22_T_46:String = "ussr-R22_T-46";

        public static const USSR_R22_T_46_BOOTCAMP:String = "ussr-R22_T-46_bootcamp";

        public static const USSR_R23_T_43:String = "ussr-R23_T-43";

        public static const USSR_R24_SU_76:String = "ussr-R24_SU-76";

        public static const USSR_R25_GAZ_74B:String = "ussr-R25_GAZ-74b";

        public static const USSR_R26_SU_8:String = "ussr-R26_SU-8";

        public static const USSR_R27_SU_14:String = "ussr-R27_SU-14";

        public static const USSR_R31_VALENTINE_LL:String = "ussr-R31_Valentine_LL";

        public static const USSR_R31_VALENTINE_LL_IGR:String = "ussr-R31_Valentine_LL_IGR";

        public static const USSR_R32_MATILDA_II_LL:String = "ussr-R32_Matilda_II_LL";

        public static const USSR_R33_CHURCHILL_LL:String = "ussr-R33_Churchill_LL";

        public static const USSR_R34_BT_SV:String = "ussr-R34_BT-SV";

        public static const USSR_R38_KV_220:String = "ussr-R38_KV-220";

        public static const USSR_R38_KV_220_BETA:String = "ussr-R38_KV-220_beta";

        public static const USSR_R39_KV_3:String = "ussr-R39_KV-3";

        public static const USSR_R40_T_54:String = "ussr-R40_T-54";

        public static const USSR_R41_T_50:String = "ussr-R41_T-50";

        public static const USSR_R42_T_60:String = "ussr-R42_T-60";

        public static const USSR_R43_T_70:String = "ussr-R43_T-70";

        public static const USSR_R43_T_70_BOOTCAMP:String = "ussr-R43_T-70_bootcamp";

        public static const USSR_R44_T80:String = "ussr-R44_T80";

        public static const USSR_R45_IS_7:String = "ussr-R45_IS-7";

        public static const USSR_R45_IS_7_FALLOUT:String = "ussr-R45_IS-7_fallout";

        public static const USSR_R45_IS_7_IGR:String = "ussr-R45_IS-7_IGR";

        public static const USSR_R46_KV_13:String = "ussr-R46_KV-13";

        public static const USSR_R47_ISU_152:String = "ussr-R47_ISU-152";

        public static const USSR_R47_ISU_152_IGR:String = "ussr-R47_ISU-152_IGR";

        public static const USSR_R49_SU100_Y:String = "ussr-R49_SU100Y";

        public static const USSR_R50_SU76_I:String = "ussr-R50_SU76I";

        public static const USSR_R50_SU76_I_BOOTCAMP:String = "ussr-R50_SU76I_bootcamp";

        public static const USSR_R51_OBJECT_212:String = "ussr-R51_Object_212";

        public static const USSR_R52_OBJECT_261:String = "ussr-R52_Object_261";

        public static const USSR_R53_OBJECT_704:String = "ussr-R53_Object_704";

        public static const USSR_R54_KV_5:String = "ussr-R54_KV-5";

        public static const USSR_R54_KV_5_IGR:String = "ussr-R54_KV-5_IGR";

        public static const USSR_R56_T_127:String = "ussr-R56_T-127";

        public static const USSR_R57_A43:String = "ussr-R57_A43";

        public static const USSR_R58_SU_101:String = "ussr-R58_SU-101";

        public static const USSR_R59_A44:String = "ussr-R59_A44";

        public static const USSR_R60_OBJECT416:String = "ussr-R60_Object416";

        public static const USSR_R61_OBJECT252:String = "ussr-R61_Object252";

        public static const USSR_R61_OBJECT252_BF:String = "ussr-R61_Object252_BF";

        public static const USSR_R61_OBJECT252_FL:String = "ussr-R61_Object252_FL";

        public static const USSR_R63_ST_I:String = "ussr-R63_ST_I";

        public static const USSR_R66_SU_26:String = "ussr-R66_SU-26";

        public static const USSR_R67_M3_LL:String = "ussr-R67_M3_LL";

        public static const USSR_R68_A_32:String = "ussr-R68_A-32";

        public static const USSR_R70_T_50_2:String = "ussr-R70_T_50_2";

        public static const USSR_R71_IS_2_B:String = "ussr-R71_IS_2B";

        public static const USSR_R72_T150:String = "ussr-R72_T150";

        public static const USSR_R73_KV4:String = "ussr-R73_KV4";

        public static const USSR_R74_SU100_M1:String = "ussr-R74_SU100M1";

        public static const USSR_R75_SU122_54:String = "ussr-R75_SU122_54";

        public static const USSR_R77_KV2:String = "ussr-R77_KV2";

        public static const USSR_R77_KV2_IGR:String = "ussr-R77_KV2_IGR";

        public static const USSR_R78_SU_85_I:String = "ussr-R78_SU_85I";

        public static const USSR_R80_KV1:String = "ussr-R80_KV1";

        public static const USSR_R81_IS8:String = "ussr-R81_IS8";

        public static const USSR_R84_TETRARCH_LL:String = "ussr-R84_Tetrarch_LL";

        public static const USSR_R86_LTP:String = "ussr-R86_LTP";

        public static const USSR_R86_LTP_BOOTCAMP:String = "ussr-R86_LTP_bootcamp";

        public static const USSR_R87_T62_A:String = "ussr-R87_T62A";

        public static const USSR_R87_T62_A_FALLOUT:String = "ussr-R87_T62A_fallout";

        public static const USSR_R88_OBJECT268:String = "ussr-R88_Object268";

        public static const USSR_R89_SU122_44:String = "ussr-R89_SU122_44";

        public static const USSR_R90_IS_4_M:String = "ussr-R90_IS_4M";

        public static const USSR_R91_SU14_1:String = "ussr-R91_SU14_1";

        public static const USSR_R93_OBJECT263:String = "ussr-R93_Object263";

        public static const USSR_R93_OBJECT263_B:String = "ussr-R93_Object263B";

        public static const USSR_R95_OBJECT_907:String = "ussr-R95_Object_907";

        public static const USSR_R95_OBJECT_907_A:String = "ussr-R95_Object_907A";

        public static const USSR_R96_OBJECT_430:String = "ussr-R96_Object_430";

        public static const USSR_R96_OBJECT_430_B:String = "ussr-R96_Object_430B";

        public static const USSR_R97_OBJECT_140:String = "ussr-R97_Object_140";

        public static const USSR_R97_OBJECT_140_BOB:String = "ussr-R97_Object_140_bob";

        public static const USSR_R98_T44_85:String = "ussr-R98_T44_85";

        public static const USSR_R98_T44_85_M:String = "ussr-R98_T44_85M";

        public static const USSR_R99_T44_122:String = "ussr-R99_T44_122";

        public static const VEHICLE_ACTION_MARKER_HUNTING:String = "vehicleActionMarker_hunting";

        public static const WHEEL:String = "wheel";

        public static const WHEEL_ORANGE:String = "wheel_orange";

        public static const WHEEL_RED:String = "wheel_red";

        public static const WHITE_CENTER:String = "whiteCenter";

        public static const WHITE_EDGE:String = "whiteEdge";

        public static const WHITE_ICON_AT_SPG16X16:String = "whiteIconAt-SPG16x16";

        public static const WHITE_ICON_HEAVY_TANK16X16:String = "whiteIconHeavyTank16x16";

        public static const WHITE_ICON_LIGHT_TANK16X16:String = "whiteIconLightTank16x16";

        public static const WHITE_ICON_MEDIUM_TANK16X16:String = "whiteIconMediumTank16x16";

        public static const WHITE_ICON_SPG16X16:String = "whiteIconSPG16x16";

        public static const YELLOW_ATSPG_H:String = "yellow_atspg_h";

        public static const YELLOW_ATSPG_M:String = "yellow_atspg_m";

        public static const YELLOW_SPG_H:String = "yellow_spg_h";

        public static const YELLOW_SPG_M:String = "yellow_spg_m";

        public static const YELLOW_TANK_SH:String = "yellow_tank_sh";

        public static const YELLOW_TANK_SH_DEAD:String = "yellow_tank_sh_dead";

        public static const YELLOW_AT_SPG_DESTROYED:String = "yellowAT-SPGDestroyed";

        public static const YELLOW_AT_SPG_NORMAL:String = "yellowAT-SPGNormal";

        public static const YELLOWHEAVY_TANK_DESTROYED:String = "yellowheavyTankDestroyed";

        public static const YELLOWHEAVY_TANK_NORMAL:String = "yellowheavyTankNormal";

        public static const YELLOWLIGHT_TANK_DESTROYED:String = "yellowlightTankDestroyed";

        public static const YELLOWLIGHT_TANK_NORMAL:String = "yellowlightTankNormal";

        public static const YELLOWMEDIUM_TANK_DESTROYED:String = "yellowmediumTankDestroyed";

        public static const YELLOWMEDIUM_TANK_NORMAL:String = "yellowmediumTankNormal";

        public static const YELLOW_SPG_DESTROYED:String = "yellowSPGDestroyed";

        public static const YELLOW_SPG_NORMAL:String = "yellowSPGNormal";

        public static const LEVEL_ENUM:Array = [LEVEL1,LEVEL10,LEVEL2,LEVEL3,LEVEL4,LEVEL5,LEVEL6,LEVEL7,LEVEL8,LEVEL9];

        public static const SQUAD_GOLD_ENUM:Array = [SQUAD_GOLD_1,SQUAD_GOLD_10,SQUAD_GOLD_11,SQUAD_GOLD_12,SQUAD_GOLD_13,SQUAD_GOLD_14,SQUAD_GOLD_2,SQUAD_GOLD_3,SQUAD_GOLD_4,SQUAD_GOLD_5,SQUAD_GOLD_6,SQUAD_GOLD_7,SQUAD_GOLD_8,SQUAD_GOLD_9];

        public static const SQUAD_SILVER_ENUM:Array = [SQUAD_SILVER_1,SQUAD_SILVER_10,SQUAD_SILVER_11,SQUAD_SILVER_12,SQUAD_SILVER_13,SQUAD_SILVER_14,SQUAD_SILVER_2,SQUAD_SILVER_3,SQUAD_SILVER_4,SQUAD_SILVER_5,SQUAD_SILVER_6,SQUAD_SILVER_7,SQUAD_SILVER_8,SQUAD_SILVER_9];

        public static const FULL_STATS_VEHICLE_TYPE_ENUM:Array = [FULL_STATS_VEHICLE_TYPE_GREEN_AT_SPG,FULL_STATS_VEHICLE_TYPE_GREEN_HEAVY_TANK,FULL_STATS_VEHICLE_TYPE_GREEN_LIGHT_TANK,FULL_STATS_VEHICLE_TYPE_GREEN_MEDIUM_TANK,FULL_STATS_VEHICLE_TYPE_GREEN_SPG,FULL_STATS_VEHICLE_TYPE_PURPLE_AT_SPG,FULL_STATS_VEHICLE_TYPE_PURPLE_HEAVY_TANK,FULL_STATS_VEHICLE_TYPE_PURPLE_LIGHT_TANK,FULL_STATS_VEHICLE_TYPE_PURPLE_MEDIUM_TANK,FULL_STATS_VEHICLE_TYPE_PURPLE_SPG,FULL_STATS_VEHICLE_TYPE_RED_AT_SPG,FULL_STATS_VEHICLE_TYPE_RED_HEAVY_TANK,FULL_STATS_VEHICLE_TYPE_RED_LIGHT_TANK,FULL_STATS_VEHICLE_TYPE_RED_MEDIUM_TANK,FULL_STATS_VEHICLE_TYPE_RED_SPG];

        public static const ALL_FALLOUT_SCORE_PANEL_ARROW_ENUM:Array = [GREEN_FALLOUT_SCORE_PANEL_ARROW,PURPLE_FALLOUT_SCORE_PANEL_ARROW,RED_FALLOUT_SCORE_PANEL_ARROW];

        public static const VEHICLE_ACTION_MARKER_ENUM:Array = [VEHICLE_ACTION_MARKER_HUNTING];

        public static const CRUISE_ENUM:Array = [CRUISE_0,CRUISE_1,CRUISE_2,CRUISE_3,CRUISE_4,CRUISE_5];

        public static const ICON_RANK_ALL_ALL_24X24_ENUM:Array = [ICON_RANK_0_0_24X24,ICON_RANK_0_1_24X24,ICON_RANK_0_10_24X24,ICON_RANK_0_11_24X24,ICON_RANK_0_12_24X24,ICON_RANK_0_13_24X24,ICON_RANK_0_14_24X24,ICON_RANK_0_15_24X24,ICON_RANK_0_2_24X24,ICON_RANK_0_3_24X24,ICON_RANK_0_4_24X24,ICON_RANK_0_5_24X24,ICON_RANK_0_6_24X24,ICON_RANK_0_7_24X24,ICON_RANK_0_8_24X24,ICON_RANK_0_9_24X24,ICON_RANK_1_0_24X24,ICON_RANK_1_1_24X24,ICON_RANK_1_10_24X24,ICON_RANK_1_11_24X24,ICON_RANK_1_12_24X24,ICON_RANK_1_13_24X24,ICON_RANK_1_14_24X24,ICON_RANK_1_15_24X24,ICON_RANK_1_2_24X24,ICON_RANK_1_3_24X24,ICON_RANK_1_4_24X24,ICON_RANK_1_5_24X24,ICON_RANK_1_6_24X24,ICON_RANK_1_7_24X24,ICON_RANK_1_8_24X24,ICON_RANK_1_9_24X24,ICON_RANK_2_0_24X24,ICON_RANK_2_1_24X24,ICON_RANK_2_10_24X24,ICON_RANK_2_11_24X24,ICON_RANK_2_12_24X24,ICON_RANK_2_13_24X24,ICON_RANK_2_14_24X24,ICON_RANK_2_15_24X24,ICON_RANK_2_2_24X24,ICON_RANK_2_3_24X24,ICON_RANK_2_4_24X24,ICON_RANK_2_5_24X24,ICON_RANK_2_6_24X24,ICON_RANK_2_7_24X24,ICON_RANK_2_8_24X24,ICON_RANK_2_9_24X24,ICON_RANK_3_0_24X24,ICON_RANK_3_1_24X24,ICON_RANK_3_10_24X24,ICON_RANK_3_11_24X24,ICON_RANK_3_12_24X24,ICON_RANK_3_13_24X24,ICON_RANK_3_14_24X24,ICON_RANK_3_15_24X24,ICON_RANK_3_2_24X24,ICON_RANK_3_3_24X24,ICON_RANK_3_4_24X24,ICON_RANK_3_5_24X24,ICON_RANK_3_6_24X24,ICON_RANK_3_7_24X24,ICON_RANK_3_8_24X24,ICON_RANK_3_9_24X24,ICON_RANK_4_0_24X24];

        public static const ICON_RANKS_GROUP_ALL_ALL_24X24_ENUM:Array = [ICON_RANKS_GROUP_0_1_24X24,ICON_RANKS_GROUP_0_2_24X24,ICON_RANKS_GROUP_0_3_24X24,ICON_RANKS_GROUP_1_1_24X24,ICON_RANKS_GROUP_1_2_24X24,ICON_RANKS_GROUP_1_3_24X24,ICON_RANKS_GROUP_2_1_24X24,ICON_RANKS_GROUP_2_2_24X24,ICON_RANKS_GROUP_2_3_24X24,ICON_RANKS_GROUP_3_1_24X24,ICON_RANKS_GROUP_3_2_24X24,ICON_RANKS_GROUP_3_3_24X24];

        public function BATTLEATLAS()
        {
            super();
        }

        public static function level(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "level" + param1;
            if(LEVEL_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[level]:atlas key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function squad_gold(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "squad_gold_" + param1;
            if(SQUAD_GOLD_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[squad_gold]:atlas key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function squad_silver(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "squad_silver_" + param1;
            if(SQUAD_SILVER_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[squad_silver]:atlas key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function getFullStatsVehicleType(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "fullStatsVehicleType_" + param1;
            if(FULL_STATS_VEHICLE_TYPE_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[getFullStatsVehicleType]:atlas key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function getFalloutScorePanelArrowByColor(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = param1 + "FalloutScorePanelArrow";
            if(ALL_FALLOUT_SCORE_PANEL_ARROW_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[getFalloutScorePanelArrowByColor]:atlas key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function getVehicleActionMarker(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "vehicleActionMarker_" + param1;
            if(VEHICLE_ACTION_MARKER_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[getVehicleActionMarker]:atlas key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function cruise(param1:String) : String
        {
            var _loc2_:String = null;
            _loc2_ = "Cruise_" + param1;
            if(CRUISE_ENUM.indexOf(_loc2_) == -1)
            {
                DebugUtils.LOG_WARNING("[cruise]:atlas key \"" + _loc2_ + "\" was not found");
                return null;
            }
            return _loc2_;
        }

        public static function getRankIcon(param1:String, param2:String) : String
        {
            var _loc3_:String = null;
            _loc3_ = "icon_rank_" + param1 + "_" + param2 + "_24x24";
            if(ICON_RANK_ALL_ALL_24X24_ENUM.indexOf(_loc3_) == -1)
            {
                DebugUtils.LOG_WARNING("[getRankIcon]:atlas key \"" + _loc3_ + "\" was not found");
                return null;
            }
            return _loc3_;
        }

        public static function getRanksGroupIcon(param1:String, param2:String) : String
        {
            var _loc3_:String = null;
            _loc3_ = "icon_ranks_group_" + param1 + "_" + param2 + "_24x24";
            if(ICON_RANKS_GROUP_ALL_ALL_24X24_ENUM.indexOf(_loc3_) == -1)
            {
                DebugUtils.LOG_WARNING("[getRanksGroupIcon]:atlas key \"" + _loc3_ + "\" was not found");
                return null;
            }
            return _loc3_;
        }
    }
}
