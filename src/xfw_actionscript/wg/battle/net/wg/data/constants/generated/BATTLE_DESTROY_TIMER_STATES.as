package net.wg.data.constants.generated
{
    public class BATTLE_DESTROY_TIMER_STATES extends Object
    {

        public static const DROWN:int = 0;

        public static const DEATH_ZONE:int = 1;

        public static const OVERTURNED:int = 2;

        public static const FIRE:int = 3;

        public static const GAS_ATTACK:int = 4;

        public static const UNDER_FIRE:int = 4;

        public static const SECTOR_AIRSTRIKE:int = 5;

        public static const RECOVERY:int = 6;

        public static const STUN:int = 10;

        public static const CAPTURE_BLOCK:int = 11;

        public static const SMOKE:int = 12;

        public static const INSPIRE:int = 13;

        public static const INSPIRE_CD:int = 14;

        public static const WARNING_VIEW:int = 0;

        public static const CRITICAL_VIEW:int = 1;

        public function BATTLE_DESTROY_TIMER_STATES()
        {
            super();
        }
    }
}
