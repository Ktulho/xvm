package net.wg.data.constants.generated
{
    public class BATTLE_VIEW_ALIASES extends Object
    {

        public static const BATTLE_LOADING:String = "battleLoading";

        public static const TEAM_BASES_PANEL:String = "teamBasesPanel";

        public static const VEHICLE_MESSAGES:String = "battleVehicleMessages";

        public static const VEHICLE_ERROR_MESSAGES:String = "battleVehicleErrorMessages";

        public static const PLAYER_MESSAGES:String = "battlePlayerMessages";

        public static const DAMAGE_PANEL:String = "damagePanel";

        public static const BATTLE_STATISTIC_DATA_CONTROLLER:String = "battleStatisticDataController";

        public static const FRAG_CORRELATION_BAR:String = "fragCorrelationBar";

        public static const BATTLE_TIMER:String = "battleTimer";

        public static const PREBATTLE_TIMER:String = "prebattleTimer";

        public static const DEBUG_PANEL:String = "debugPanel";

        public static const SIXTH_SENSE:String = "sixthSense";

        public static const DAMAGE_INFO_PANEL:String = "damageInfoPanel";

        public static const BATTLE_MESSENGER:String = "battleMessenger";

        public static const BATTLE_TUTORIAL:String = "battleTutorial";

        public static const CONSUMABLES_PANEL:String = "consumablesPanel";

        public static const DAMAGE_INDICATOR:String = "damageIndicator";

        public static const DESTROY_TIMERS_PANEL:String = "destroyTimersPanel";

        public static const DUAL_GUN_PANEL:String = "dualGunPanel";

        public static const REPORT_BUG:String = "reportBug";

        public static const FULL_STATS:String = "fullStats";

        public static const RIBBONS_PANEL:String = "ribbonsPanel";

        public static const MINIMAP:String = "minimap";

        public static const RADIAL_MENU:String = "radialMenu";

        public static const PLAYERS_PANEL:String = "playersPanel";

        public static const TICKER:String = "battleTicker";

        public static const REPAIR_POINT_TIMER:String = "repairPointTimer";

        public static const FLAG_NOTIFICATION:String = "flagNotification";

        public static const BATTLE_END_WARNING_PANEL:String = "battleEndWarningPanel";

        public static const POSTMORTEM_PANEL:String = "postmortemPanel";

        public static const BATTLE_DAMAGE_LOG_PANEL:String = "battleDamageLogPanel";

        public static const CROSSHAIR_PANEL:String = "crosshairPanel";

        public static const MARKERS_2D:String = "markers2D";

        public static const HIT_DIRECTION:String = "hitDirection";

        public static const SIEGE_MODE_INDICATOR:String = "siegeModeIndicator";

        public static const BOOTCAMP_CONSUMABLES_PANEL:String = "bootcampConsumablesPanel";

        public static const BOOTCAMP_RIBBONS_PANEL:String = "bootcampRibbonsPanel";

        public static const BOOTCAMP_SECONDARY_HINT:String = "bootcampSecondaryHint";

        public static const BOOTCAMP_BATTLE_TOP_HINT:String = "bootcampBattleTopHint";

        public static const GAME_MESSAGES_PANEL:String = "gameMessagesPanel";

        public static const EPIC_RANDOM_SCORE_PANEL:String = "epicRandomScorePanel";

        public static const EPIC_RESPAWN_VIEW:String = "epicRespawnView";

        public static const EPIC_SCORE_PANEL:String = "epicScorePanel";

        public static const EPIC_TIMER:String = "epicTimer";

        public static const EPIC_DEPLOYMENT_MAP:String = "epicDeploymentMap";

        public static const EPIC_OVERVIEW_MAP_SCREEN:String = "epicOverviewMapScreen";

        public static const EPIC_MISSIONS_PANEL:String = "epicMissionsPanel";

        public static const EPIC_SPECTATOR_VIEW:String = "epicSpectatorView";

        public static const EPIC_REINFORCEMENT_PANEL:String = "epicReinforcementPanel";

        public static const RECOVERY_PANEL:String = "recoveryPanel";

        public static const EPIC_INGAME_RANK:String = "epicInGameRank";

        public static const SUPER_PLATOON_PANEL:String = "superPlatoonPanel";

        public static const BATTLE_TANK_CAROUSEL:String = "battleTankCarousel";

        public static const BATTLE_TANK_CAROUSEL_FILTER_POPOVER:String = "battleTankCarouselFilterPopover";

        public static const QUEST_PROGRESS_TOP_VIEW:String = "questProgressTopView";

        public static const HINT_PANEL:String = "battleHintPanel";

        public static const HELP_DETAILED:String = "helpDetailed";

        public function BATTLE_VIEW_ALIASES()
        {
            super();
        }
    }
}
