package net.wg.gui.battle.views.dualGunPanel
{
    public class DualGunPanelState extends Object
    {

        public static const SINGLE_SHOT_MODE:String = "singleMode";

        public static const READY_FOR_CHARGE:String = "readyForCharge";

        public static const CHARGE:String = "charge";

        public static const CANCEL_CHARGE:String = "cancelCharge";

        public static const COOLDOWN:String = "cooldown";

        public static const COLLAPSE:String = "collapse";

        public static const EXPAND:String = "expand";

        public static const CHARGED:String = "charged";

        public function DualGunPanelState()
        {
            super();
        }
    }
}
