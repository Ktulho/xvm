package net.wg.gui.battle.views.damagePanel.VO
{
    public class TooltipStringByItemStateVO extends Object
    {

        public var critical:String = "";

        public var destroyed:String = "";

        public var normal:String = "";

        public function TooltipStringByItemStateVO()
        {
            super();
        }
    }
}
