package net.wg.gui.battle.components.buttons.interfaces
{
    public interface ITooltipTarget
    {

        function get tooltipStr() : String;

        function set tooltipStr(param1:String) : void;
    }
}
